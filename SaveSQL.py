# module qui permet d'importer une base de donnée
import sqlite3

import cv2
import keyboard
import matplotlib.pyplot as plt
#from Joueur import Joueur

#Création de la base de donnée
def createDB():
    baseDeDonnees = sqlite3.connect('jeu.db') # BaseDeDonnee représente la base de donnée et porte le nom jeu
    curseur = baseDeDonnees.cursor() # le curseur permet d'executer du code dans la base de donnée
    # Création de la base de données DartData
    # Creation des colonnes de la table
    curseur.execute("CREATE TABLE IF NOT EXISTS DartData (id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT NOT NULL, partie INTEGER NOT NULL, fPartie INTEGER NOT NULL, fRound INTEGER NOT NULL,fPoint INTEGER NOT NULL,x INTEGER NOT NULL,y INTEGER NOT NULL)")
    # Création de la base de données PlayerData
    # Creation des colonnes de la table
    curseur.execute("CREATE TABLE IF NOT EXISTS PlayerData (id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT NOT NULL, partie_win INTEGER NOT NULL, partie_total INTEGER NOT NULL,num_Flechette INTEGER NOT NULL,nbre_P_Moy FLOAT NOT NULL)")
    baseDeDonnees.close()

def createDB_Joueur():
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("CREATE TABLE IF NOT EXISTS PlayerData (id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT NOT NULL, partie_win INTEGER NOT NULL, partie_total INTEGER NOT NULL,num_Flechette INTEGER NOT NULL,nbre_P_Moy FLOAT NOT NULL)") # Création de la base de données
    baseDeDonnees.close()

def deleteDB_Joueur(nom):
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("DELETE FROM PlayerData WHERE nom = ?", (nom,))
    curseur.execute("DELETE FROM DartData WHERE nom = ?", (nom,))
    baseDeDonnees.commit()

def deleteDataB():
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("DELETE FROM DartData")
    curseur.execute("DELETE FROM PlayerData")
    baseDeDonnees.commit()

#Insertion de toutes les données du joueur
def addData(nom,partie,fPartie, fRound,fPoint,x,y):
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("INSERT INTO DartData (nom, partie, fPartie, fRound,fPoint,x,y) VALUES (?, ?, ?, ?,?,?,?)", (nom,partie,fPartie, fRound,fPoint,x,y))  # On ajoute un enregistrement
    baseDeDonnees.commit()
    baseDeDonnees.close()

def addColuDB():
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("ALTER TABLE DartData ADD x INTEGER NOT NULL CONSTRAINT valeur_par_defaut DEFAULT 0")
    curseur.execute("ALTER TABLE DartData ADD y INTEGER NOT NULL CONSTRAINT valeur_par_defaut DEFAULT 0")
    baseDeDonnees.commit()
    baseDeDonnees.close()

def addColuDB_Joueur():
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("ALTER TABLE PlayerData ADD num_Flechette INTEGER NOT NULL CONSTRAINT valeur_par_defaut DEFAULT 0")
    baseDeDonnees.commit()
    baseDeDonnees.close()

def addColuDB_Joueur2():
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("ALTER TABLE PlayerData ADD nbre_P_Moy FLOAT NOT NULL CONSTRAINT valeur_par_defaut DEFAULT 0")
    baseDeDonnees.commit()
    baseDeDonnees.close()

def addJoueur_Joueur(nom,num_flechette):
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("INSERT INTO PlayerData (nom, partie_win, partie_total,num_Flechette,nbre_P_Moy) VALUES (?, ?, ?,?,?)", (nom,0,0,num_flechette,0))  # On ajoute un enregistrement
    baseDeDonnees.commit() # enregistre les modifications
    baseDeDonnees.close()

def verif_num_flech(nom):

    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("SELECT num_Flechette FROM PlayerData WHERE nom = ?", (nom,))
    # Pour pouvoir utiliser une variable dans un string, les points d'interrogations sont remplacés dans l'ordre par la ou les variables en fin de parenthèse
    text = curseur.fetchone()
    if text!=None:
        num=text[0]
    else: num=None
    return num

def def_num_flechette(nom,num):
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("UPDATE PlayerData SET num_Flechette = ? WHERE nom = ?", (num, nom,))
    baseDeDonnees.commit()
    baseDeDonnees.close()

def add_nbre_P_Moy(nom,nbre_P):
    #nbre_P correspond au nombre de personne qu'il y avait dans la partie
    nbreMoyen=get_nbre_P_Moy(nom)
    nbrePart=getPartie(nom)
    # Le nouveau nombre de personne moyenne :
    print("Nombre Personne Moy:", nbreMoyen,", nombre de partie :",nbrePart,", nbre_P :",nbre_P)
    new_nbreMoy=((nbreMoyen*nbrePart)+nbre_P)/(nbrePart+1)
    print("nouveau nombre de personne : ",new_nbreMoy)
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("UPDATE PlayerData SET nbre_P_Moy = ? WHERE nom = ?", (new_nbreMoy, nom,))
    baseDeDonnees.commit()
    baseDeDonnees.close()

def addVictory_Joueur(nom,nbre_Player):
    add_nbre_P_Moy(nom, nbre_Player)
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    nbrV=getVictory(nom)
    nbrV+=1
    nbreP = getPartie(nom)
    nbreP += 1
    curseur.execute("UPDATE PlayerData SET partie_win = ?, partie_total=? WHERE nom = ?", (nbrV,nbreP,nom,))
    baseDeDonnees.commit()
    baseDeDonnees.close()


def addLose_Joueur(nom,nbre_Player):
    add_nbre_P_Moy(nom, nbre_Player)
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    nbrP = getPartie(nom)
    nbrP += 1
    curseur.execute("UPDATE PlayerData SET partie_total=? WHERE nom = ?", (nbrP,nom,))
    baseDeDonnees.commit()
    baseDeDonnees.close()



def getVictory(nom):
    baseDeDonnees = sqlite3.connect('jeu.db')
    cursor = baseDeDonnees.cursor()
    cursor.execute("SELECT partie_win FROM PlayerData WHERE nom=?",(nom,))
    victoire = cursor.fetchone()
    vict = victoire[0]
    return vict

def getPartie(nom):
    baseDeDonnees = sqlite3.connect('jeu.db')
    cursor = baseDeDonnees.cursor()
    cursor.execute("SELECT partie_total FROM PlayerData WHERE nom=?",(nom,))
    nbrPartie = cursor.fetchone()
    nbre_P=nbrPartie[0]
    return nbre_P

def get_nbre_P_Moy(nom):
    baseDeDonnees = sqlite3.connect('jeu.db')
    cursor = baseDeDonnees.cursor()
    cursor.execute("SELECT nbre_P_Moy FROM PlayerData WHERE nom=?",(nom,))
    nbrMoy = cursor.fetchone()
    nbre_Moy=nbrMoy[0]
    return nbre_Moy

def get_all_numflech():
    data = []
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("SELECT num_Flechette FROM PlayerData")
    for i in curseur.fetchall():
        data.append(i[0])
    baseDeDonnees.commit()
    baseDeDonnees.close()
    return data

# Sortie d'un tableau contenant toutes les informations du joueur
def getData(nom):
    data=[]
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("SELECT partie,fPartie, fRound,fPoint,x,y FROM DartData WHERE nom = ?", (nom,))
    for i in curseur.fetchall():
        data.append(i)
    baseDeDonnees.commit()
    baseDeDonnees.close()
    return data

def get_num_partie(nom):
    data = []
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("SELECT partie FROM DartData WHERE nom = ?", (nom,))
    for i in curseur.fetchall():
        data.append(i[0])
    baseDeDonnees.commit()
    baseDeDonnees.close()
    print("numéro de partie :",data)
    max_value = max(data)
    print("Valeur max :",max_value)
    return max_value

def getAllPlayer():
    data = []
    data2 = []
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    curseur.execute("SELECT nom FROM DartData")
    first=True
    for i in curseur.fetchall(): # La méthode Fetchall est une alternative à Fetchone, qui permet de récupérer d'un coup l'ensemble du résultat d'une requête stockée dans un vecteur
        data.append(i)
    data=filterData(data) # filterData est une méthode définie juste en dessous et permet Trie data pour n'en ressortir une liste ou les noms ne sont present qu'une fois
    print("Noms trouvés dans la table DartData :",data)
    curseur.execute("SELECT nom FROM PlayerData")
    for i in curseur.fetchall():
        data2.append(i)
    print("Noms trouvés dans la table PlayerData :", data2)
    if len(data)!=len(data2):
        print("ERREUR, la longueur des listes de joueur dans les tables de la BD sont différentes")
    baseDeDonnees.commit() # sauvegarde des changements
    baseDeDonnees.close()
    return data

#Trie data pour n'en ressortir une liste ou les noms ne sont present qu'une fois
def filterData(data):
    newData=[]
    for i in range(len(data)):
        new=True
        for j in range(len(newData)):
            if data[i]==newData[j]:
                new=False
        if new==True:
            newData.append(data[i])
    return newData



def showPartie(nom):
    plt.title("Déroulement des parties (appuyer sur q pour quitter)")
    # Créer un tableau qui simule le déroulement de la partie
    # le tableau reprendra les fléchettes dans l'ordre et doit plot dés qu'une partie est terminée
    # on récupère les infos sous la forme :partie,fPartie, fRound,fPoint
    data=getData(nom)
    partie=[]
    npartie=1
    for i in range(len(data)):
        #print("partie = ", npartie, ", partiedata =", data[i][0])
        if npartie==int(data[i][0]):
            #print("Ajoute dans ",data[i][0]," ,point =",data[i][3]," ,lancé =",data[i][1])
            partie.append((data[i][3],data[i][1]))
            if i+1==len(data):
                #print("partie=", partie)
                plt.plot(partie)
        elif npartie != int(data[i][0]):
            #print("change dans ", data[i][0]," car partie = ",npartie,", le tableau =",partie)
            plt.plot(partie)
            partie.clear()
            npartie+=1
            partie.append((data[i][3], data[i][1]))
    plt.xlabel('Lancé')
    plt.ylabel('Point')

    plt.show()
    cv2.waitKey(1000)
    while True:
        if keyboard.read_key() == "q":
            plt.close()
            break
def show_f(nom,num):
    print("Show flech",num)
    plt.title("Flech Round (appuyer sur q pour quitter)")
    # on récupère les infos sous la forme :partie,fPartie, fRound,fPoint
    data = getData(nom)
    print(data)
    tab_score=[]
    for i in range(len(data)):
        if data[i][2]==num:
            tab_score.append(data[i][3])
    print(tab_score)
    n, bins, patches = plt.hist(tab_score)

    plt.xlabel('flechette')
    plt.ylabel(u'score')

    plt.grid(True)
    plt.show()
    cv2.waitKey(1000)
    while True:
        if keyboard.read_key() == "q":
            plt.close()
            break

def fusionPlayer(nom1,nom2):
    baseDeDonnees = sqlite3.connect('jeu.db')
    curseur = baseDeDonnees.cursor()
    text="UPDATE DartData SET nom = '"+nom1+"' WHERE nom ='"+nom2+"'"
    #print(text)
    curseur.execute(text)
    baseDeDonnees.commit()
    baseDeDonnees.close()