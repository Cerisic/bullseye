import cv2
import cv2.aruco as aruco
import numpy as np
import MultiCam
from scipy import ndimage
import Jeu
import ImageAnalysis


def verifRef(arucoRef, camNum): # permet de vérifier que la caméra 1 détecte au moins 2 détecteurs de référence constituant le plan de la cible
    print("Entre dans VerifRef")
    # Regarder les arucos de reference visibles et incrémenter dans le tableau que ceux visible
    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
    arucoParameters = aruco.DetectorParameters_create() # Creation de paramètre par défaut de détection des aruco
    MultiCam.enableCamera(camNum) # permet d'actionner la caméra une par une
    cap = cv2.VideoCapture(0)

    # Passer 5 images sur chaque aruco de reference pour vérifier si le aruco voulu est détecté
    arucoV = []
    initialArucoVLen = 0
    first = True
    while True:
        cv2.waitKey(100) # on attend une stabilisation de l'objectif de la caméra
        initialArucoVLen = len(arucoV)
        for j in range(len(arucoRef)): # arucoRef = [1,2,3,4]
            # On se fixe un marqueur de référence qu'il faut arriver à identifier sur 10 itérations
            increment = 0
            for i in range(10):
                # On récupère les "frames" depuis la caméra
                ret, frame = cap.read()
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                markerCorners, markerIds, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=arucoParameters)

                # Si un marqueur est détecté
                if np.all(markerIds is not None): # Teste si tous les éléments du tableau le long d'un axe donné valent True.
                    # ex: np.all([1,5,7] > 3) -> False
                    # Parcourir la liste des marqueurs vu
                    increment += 1 # l'incrément identifie le nombre de fois que le marqueur considére arucoRef[j] a été identifié sur 10 fois
                    print("increment : ", increment)
                    for i in range(len(markerIds)): # problème 2 boucles utilisent la variable i
                        # Si un marqueur correspond à la liste des arucos de référence
                        if markerIds[i][0] == arucoRef[j]:
                            # Et qu'il n'a pas déjà été vérifié
                            if first:# ne passe qu'une seule et une seule fois ici
                                arucoV.append(arucoRef[j])
                                first = False
                            if arucoRef[j] != arucoV[-1]: # arucoV[-1] signifie le dernier élément de la liste
                                # Rajouter cet aruco à la liste des arucos vu par la cam
                                arucoV.append(arucoRef[j])

                if len(arucoV) == initialArucoVLen: # si la taille des 2 tableaux sont les mêmes aucun Aruco a été ajouté à ArucoV pendant le traitement et donc le marqueur n'a pas été identifie
                    print("aruco %s non détécté..." % arucoRef[j])
                    break

        # Si au moins deux Aruco de référence ont été identifié c'est validé
        if len(arucoV) >= 2:
            print("Deux Aruco de référence détéctés")
            break

    verifSuppl = 0 # permet de vérifier si on ne détecte pas plus de 4 marqueurs de référence
    while True:
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = ndimage.rotate(gray, 90) # rotation de 90° de l'image
        markerCorners, markerIds, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=arucoParameters)
        if np.all(markerIds is not None):
            if len(markerIds) >= 4: # Si plus de 4 marqueurs Aruco ont été identifiés
                verifSuppl += 1 
                frame = ndimage.rotate(frame, 90)
                cv2.imwrite("Images/InitialCol/cam%s.jpg" %(camNum), frame)
                if verifSuppl >= 5:
                    break
            else:
                verifSuppl = 0 

    cap.release()
    print("Liste de aruco vérifiés : ", arucoV)
    corners = ImageAnalysis.ArucoCornersDetermination(frame) # appel du fichier ImageAnalysis qui permet d'identifier un coin de référence des 4 marqueurs
    unwarped_images = ImageAnalysis.warpPerspective(frame, corners, 0, 480, 640, 0, 0) # Cete méthode permet de redresser l'image
    cv2.imwrite("Images/UnwrapedImage/Init_cam%s.jpg" % (camNum), unwarped_images) # cv2.imwrite(filename, image)
    return arucoV


def localisation(camNum, numMarker):
    # camNum = numéro de la caméra pour laquelle on souhaite obtenir une image avec l'Aruco de la nouvelle fléchette

    MultiCam.enableCamera(camNum)
    cap = cv2.VideoCapture(0)

    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
    arucoParameters = aruco.DetectorParameters_create()

    increment = 0
    verif = False
    frame = []

    for a in range(20):
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = ndimage.rotate(gray, 90)
        markerCorners, markerIds, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=arucoParameters)

        if np.all(markerIds is not None):
            verif = False
            for i in range(len(markerIds)):
                if markerIds[i][0] == numMarker:
                    verif = True
            if verif:
                increment += 1

        if increment > 3:
            break

    if not verif:
        print("WARNING: La caméra %s ne voit peut-être plus l'Aruco sur la fléchette pour le moment" % camNum)
    
    """
    while True:
        print("test")
        verif = False
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = ndimage.rotate(gray, 90)
        markerCorners, markerIds, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=arucoParameters)
        if np.all(markerIds is not None):
            for i in range(len(markerIds)):
                if markerIds[i][0] == numMarker:
                    verif = True
                break
            if verif == True:
                break
    """

    cap.release()
    cv2.destroyAllWindows()
    print("Aruco %s vu" % numMarker)

    return frame


def checkNewMarker(markerIds, flechVu, engage, arucoRef):
    # Verification si une fléchette n'as pas encore été vue
    finRound = False
    newflechette = 404

    for i in range(len(markerIds)): # comprend peut-être des marqueurs qui ne font pas partie des marqueurs de référence
        new = True
        # Voir si c'est un aruco de reference
        for j in range(len(arucoRef)):
            if markerIds[i][0] == arucoRef[j]:
                new = False
        # Voir si c'est une flechette déjà vue
        for j in range(len(flechVu)):
            if markerIds[i][0] == flechVu[j]:
                new = False

        # Si oui alors newflechette prend la valeur de l'identifiant de la flechette à détecter
        if new == True:
            engage = True
            newflechette = markerIds[i][0]
            flechVu.append(newflechette)
            break

    # Si plus aucun marker est détecté et qu'il y avait des fléchettes auparavant
    # Dans ce cas-ci, on vient de retirer les différentes fléchettes pour passer au round suivante (passer au prochain joueur)
    if engage == True:
        p = 0
        for i in range(len(markerIds)):
            for j in range(len(arucoRef)):
                if markerIds[i] == arucoRef[j]:
                    p = p + 1
        if len(markerIds) == p:
            finRound = True
    return flechVu, engage, finRound, newflechette