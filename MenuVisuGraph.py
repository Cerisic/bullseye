
from PyQt5 import QtCore, QtGui, QtWidgets
import SaveSQL
from Menu_Voir import Ui_Dialog

class Ui_FormVisuG(object):
    def setupUi(self, Form,data):
        Form.setObjectName("Form")
        Form.resize(771, 437)
        self.dialog=Form
        self.comboBo_nom = QtWidgets.QComboBox(Form)
        self.comboBo_nom.setGeometry(QtCore.QRect(30, 30, 501, 71))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.comboBo_nom.setFont(font)
        self.comboBo_nom.setObjectName("comboBo_nom")
        for i in range(len(data)):
            self.comboBo_nom.addItems(data[i])
        self.pushButton_Valider = QtWidgets.QPushButton(Form)
        self.pushButton_Valider.setGeometry(QtCore.QRect(570, 30, 171, 60))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.pushButton_Valider.setFont(font)
        self.pushButton_Valider.setObjectName("pushButton_Valider")
        self.pushButton_Valider.clicked.connect(self.clicked_valider)

        self.pushButton_Voir = QtWidgets.QPushButton(Form)
        self.pushButton_Voir.setGeometry(QtCore.QRect(570, 100, 171, 60))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.pushButton_Voir.setFont(font)
        self.pushButton_Voir.setObjectName("pushButton_Voir")
        self.pushButton_Voir.clicked.connect(self.clicked_voir)

        self.pushButton_Supp = QtWidgets.QPushButton(Form)
        self.pushButton_Supp.setGeometry(QtCore.QRect(570, 180, 171, 50))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.pushButton_Supp.setFont(font)
        self.pushButton_Supp.setObjectName("pushButton_Supp")
        self.pushButton_Supp.clicked.connect(self.clicked_supp)

        self.pushButton_Supp_All = QtWidgets.QPushButton(Form)
        self.pushButton_Supp_All.setGeometry(QtCore.QRect(570, 240, 171, 50))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.pushButton_Supp_All.setFont(font)
        self.pushButton_Supp_All.setObjectName("pushButton_Supp_All")
        self.pushButton_Supp_All.clicked.connect(self.clicked_supp_all)


        self.comboBox_graph = QtWidgets.QComboBox(Form)
        self.comboBox_graph.setGeometry(QtCore.QRect(30, 130, 501, 71))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.comboBox_graph.setFont(font)
        self.comboBox_graph.setObjectName("comboBox_graph")
        self.comboBox_graph.addItems(("Graphique Partie",))
        self.comboBox_graph.addItems(("Premiere flech round",))
        self.comboBox_graph.addItems(("Deuxieme flech round",))
        self.comboBox_graph.addItems(("Troisieme flech round",))
        self.pushButton_Quitter = QtWidgets.QPushButton(Form)
        self.pushButton_Quitter.setGeometry(QtCore.QRect(590, 350, 141, 61))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.pushButton_Quitter.setFont(font)
        self.pushButton_Quitter.setObjectName("pushButton_Quitter")
        self.pushButton_Quitter.clicked.connect(self.clicked_quitter)
        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.pushButton_Valider.setText(_translate("Form", "Valider"))
        self.pushButton_Voir.setText(_translate("Form", "Voir joueur"))
        self.pushButton_Supp.setText(_translate("Form", "Supprimer"))
        self.pushButton_Supp_All.setText(_translate("Form", "Supprimer tout"))
        self.pushButton_Quitter.setText(_translate("Form", "Quitter"))
        self.pushButton_Quitter.setStyleSheet("background-color: red;")
        self.pushButton_Valider.setStyleSheet("background-color: green;")
        self.pushButton_Voir.setStyleSheet("background-color: green;")
        self.pushButton_Supp_All.setStyleSheet("background-color: red;")
        self.pushButton_Supp.setStyleSheet("background-color: red;")
        Form.setStyleSheet("background-color: yellow;")


    def clicked_valider(self):
        nom = self.comboBo_nom.currentText()
        graph= self.comboBox_graph.currentText()
        print("AFFICHAGE :",graph," de ",nom)
        if graph=="Graphique Partie":
            SaveSQL.showPartie(nom)
        if graph=="Premiere flech round":
            SaveSQL.show_f(nom,1)
        if graph=="Deuxieme flech round":
            SaveSQL.show_f(nom,2)
        if graph=="Troisieme flech round":
            SaveSQL.show_f(nom,3)

    def clicked_voir(self):
        nom = self.comboBo_nom.currentText()
        formSecond = QtWidgets.QDialog()
        ui = Ui_Dialog()
        ui.setupUi(formSecond, nom)
        ret = formSecond.exec_()
        print(" Visualisation du joueur :",nom)

    def clicked_supp(self):
        nom = self.comboBo_nom.currentText()
        SaveSQL.deleteDB_Joueur(nom)
        print(" SUPPRESSION BD de :",nom)

    def clicked_supp_all(self):
        SaveSQL.deleteDataB()
        print("SUPPRESSION ALL BD")



    def clicked_quitter(self):
        print("Fermeture Fenetre Visualiser")
        self.dialog.reject()


if __name__ == "__main__":
    import sys

    SaveSQL.createDB()
    SaveSQL.addData("Alex", 1, 1, 1, 100, 10, 20)
    SaveSQL.addData("Alex", 1, 2, 2, 120, 20, 30)
    SaveSQL.addData("Ayoub", 1, 3, 3, 110, 12, 13)
    data = SaveSQL.getAllPlayer()
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_FormVisuG()
    ui.setupUi(Form,data)
    Form.show()
    sys.exit(app.exec_())
