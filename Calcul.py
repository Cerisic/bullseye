import math
import cv2
import numpy as np


def moyenne(tab, placeTab):
    xtot = 0
    if len(tab)!=0:
        for i in range(len(tab)):
            xtot += tab[i][placeTab]
        xmoy = xtot / len(tab)
    else: print("ERREUR: Longueur du tableau nule")
    return xmoy


def compaFlechVu(newTab_flechVu, flechVu):

    for i in flechVu:
        verif = False
        for j in newTab_flechVu:
            if i == j:
                verif = True

        if verif == False:
            newTab_flechVu.append(i)

    return newTab_flechVu


def newflechetteCompa(newflechette0, newflechette1, newflechette2, newflechette3):
    newf = 404
    tabCamNum = []
    tab_camera = []

    # newf prend la valeur de la nouvelle flechette détectée
    if newflechette0 != 404:
        newf = newflechette0
        tabCamNum.append(1)
    if newflechette1 != 404:
        if newflechette1 != newf and newf != 404:
            print("ERREUR: Les newflechettes0 et 1 ne correspondent pas")
            newf = 404
        else:
            newf = newflechette1
            tabCamNum.append(2)

    return newf, tabCamNum


def SortList(list):
    cases = [[]]
    casesMoy = []
    first = 0

    for i in list:

        if first == 1:
            for j in range(len(casesMoy)):
                rentre = True
                if (casesMoy[j][0] - 0.3 > i[0]) or (casesMoy[j][0] + 0.3 < i[0]): rentre = False
                if (casesMoy[j][1] - 0.3 > i[1]) or (casesMoy[j][1] + 0.3 < i[1]): rentre = False
                if rentre == True:
                    cases[j].append(i)
                    casesMoy[j] = Moy2D(cases[j])
                    break
            if rentre == False:
                cases.append([i])
                casesMoy.append(i)
        if first == 0:
            cases[0].append(i)
            casesMoy.append(i)
            first = 1
    return cases, casesMoy


def Moy2D(vectors):
    xSum = 0
    ySum = 0
    zSum = 0

    for i in vectors:
        xSum = i[0] + xSum
        ySum = i[1] + ySum
        zSum = i[2] + zSum

    vectMoy = [xSum / len(vectors), ySum / len(vectors), zSum / len(vectors)]
    return vectMoy


def ChooseVector(cases, casesMoy):
    LMax = len(cases[0])
    z = 0
    for i in range(len(cases)):

        if len(cases[i]) >= LMax:
            LMax = len(cases[i])
            z = i
    vector = casesMoy[z]
    
    # Dans cette partie on affiche le "case" choisi
    print("CASES%s : " %(z+1))
    for i in cases[z]:
        print(i)
    print("CasesMoy : ", casesMoy[z])

    return vector


def findAmpAng(xc, yc, xp, yp, nomimage):
    # xc est la coordonnée x du centre de l'image
    # yc est la coordonnée y du centre de la cible
    x = xp - xc
    y = yp - yc
    longueur = math.sqrt(math.pow(x, 2) + math.pow(y, 2)) # Calcul la distance entre le point d'impact de l'image redressée et le centre
    print("y ", y)
    print("x ", x)
    angle = np.arctan(y / x) # permet de calculer l'angle entre l'horizontal et le point d'intersection
    angle = (angle * 180) / math.pi # conversion en degré
    print("angle avant : ", angle)
    if x < 0 and y > 0:
        angle = 180 + angle
    if x < 0 and y < 0:
        angle = 180 + angle
    if x > 0 and y < 0:
        angle = 360 + angle
    print("angle après : ", angle)
    print("longueur : ", longueur)

    image = cv2.imread(nomimage)
    cv2.line(image,(int(xc),int(yc)),(int(xp),int(yp)),(255,0,0),3)
    cv2.line(image, (int(xc), int(yc)), (np.size(image, 1), int(yc)), (255, 0, 0), 3)
    cv2.imwrite("Images/UnwrapedImage/ImageAvecLongueur.jpg", image)

    return longueur, angle


def getPoint(longueur, angle, color):
    angle2T = 36
    point = 0 # point suite au tir de la fléchette
    pRef = 50 # distance entre le centre et le cercle intérieur des points triples
    gRef = 170 # distance entre le centre et le cercle intérieur des points doubles
    if color == "noir":
        if angle2T > angle >= 0:
            point = 10
        if angle2T * 2 > angle >= angle2T:
            point = 2
        if angle2T * 3 > angle >= angle2T * 2:
            point = 3
        if angle2T * 4 > angle >= angle2T * 3:
            point = 7
        if angle2T * 5 > angle >= angle2T * 4:
            point = 8
        if angle2T * 6 > angle >= angle2T * 5:
            point = 14
        if angle2T * 7 > angle >= angle2T * 6:
            point = 12
        if angle2T * 8 > angle >= angle2T * 7:
            point = 20
        if angle2T * 9 > angle >= angle2T * 8:
            point = 18
        if angle2T * 10 > angle >= angle2T * 9:
            point = 13
    if color == "beige":
        if angle2T + 18 > angle >= 18:
            point = 15
        if (angle2T * 2) + 18 > angle >= angle2T + 18:
            point = 17
        if (angle2T * 3) + 18 > angle >= (angle2T * 2) + 18:
            point = 19
        if (angle2T * 4) + 18 > angle >= (angle2T * 3) + 18:
            point = 16
        if (angle2T * 5) + 18 > angle >= (angle2T * 4) + 18:
            point = 11
        if (angle2T * 6) + 18 > angle >= (angle2T * 5) + 18:
            point = 9
        if (angle2T * 7) + 18 > angle >= (angle2T * 6) + 18:
            point = 5
        if (angle2T * 8) + 18 > angle >= (angle2T * 7) + 18:
            point = 1
        if (angle2T * 9) + 18 > angle >= (angle2T * 8) + 18:
            point = 4
        if (angle2T * 9) + 18 < angle or angle < 18:
            point = 6
    if color == "rouge" and longueur > pRef:
        if angle2T > angle >= 0:
            if longueur < gRef:
                point = 30
            if longueur > gRef:
                point = 20
        if angle2T * 2 > angle >= angle2T:
            if longueur < gRef:
                point = 6
            if longueur > gRef:
                point = 4
        if angle2T * 3 > angle >= angle2T * 2:
            if longueur < gRef:
                point = 9
            if longueur > gRef:
                point = 6
        if angle2T * 4 > angle >= angle2T * 3:
            if longueur < gRef:
                point = 21
            if longueur > gRef:
                point = 14
        if angle2T * 5 > angle >= angle2T * 4:
            if longueur < gRef:
                point = 24
            if longueur > gRef:
                point = 16
        if angle2T * 6 > angle >= angle2T * 5:
            if longueur < gRef:
                point = 42
            if longueur > gRef:
                point = 28
        if angle2T * 7 > angle >= angle2T * 6:
            if longueur < gRef:
                point = 36
            if longueur > gRef:
                point = 24
        if angle2T * 8 > angle >= angle2T * 7:
            if longueur < gRef:
                point = 60
            if longueur > gRef:
                point = 40
        if angle2T * 9 > angle >= angle2T * 8:
            if longueur < gRef:
                point = 54
            if longueur > gRef:
                point = 36
        if angle2T * 10 > angle >= angle2T * 9:
            if longueur < gRef:
                point = 39
            if longueur > gRef:
                point = 26
    if color == "vert" and longueur > pRef:
        if angle2T + 18 > angle >= 18:
            if longueur < gRef:
                point = 45
            if longueur > gRef:
                point = 30
        if (angle2T * 2) + 18 > angle >= angle2T + 18:
            if longueur < gRef:
                point = 51
            if longueur > gRef:
                point = 34
        if (angle2T * 3) + 18 > angle >= (angle2T * 2) + 18:
            if longueur < gRef:
                point = 57
            if longueur > gRef:
                point = 38
        if (angle2T * 4) + 18 > angle >= (angle2T * 3) + 18:
            if longueur < gRef:
                point = 48
            if longueur > gRef:
                point = 32
        if (angle2T * 5) + 18 > angle >= (angle2T * 4) + 18:
            if longueur < gRef:
                point = 33
            if longueur > gRef:
                point = 22
        if (angle2T * 6) + 18 > angle >= (angle2T * 5) + 18:
            if longueur < gRef:
                point = 27
            if longueur > gRef:
                point = 18
        if (angle2T * 7) + 18 > angle >= (angle2T * 6) + 18:
            if longueur < gRef:
                point = 15
            if longueur > gRef:
                point = 10
        if (angle2T * 8) + 18 > angle >= (angle2T * 7) + 18:
            if longueur < gRef:
                point = 3
            if longueur > gRef:
                point = 2
        if (angle2T * 9) + 18 > angle >= (angle2T * 8) + 18:
            if longueur < gRef:
                point = 12
            if longueur > gRef:
                point = 8
        if (angle2T * 9) + 18 < angle or angle < 18:
            if longueur < gRef:
                point = 18
            if longueur > gRef:
                point = 12
    if color == "vert" and longueur < pRef:
        point = 25
    if color == "rouge" and longueur < pRef:
        point = 50
    return point