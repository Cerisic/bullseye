import cv2
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import argparse
import utils
import cv2
import numpy as np
from numpy import linalg as LA
import cv2
#import cv2.aruco as aruco
import math
import keyboard
from PIL.Image import Image
from PIL import Image
import os
from sympy import symbols, simplify, sin, cos, Eq, linsolve, solve
from scipy import ndimage
import Demo

def afficherPoint(xa, ya, nomImage, couleur):
    image = cv2.imread(nomImage)
    r = 255
    v = 255
    b = 255
    if couleur == "B":
        b = 255
        v = 0
        r = 0
    if couleur == "V":
        r = 0
        v = 255
        b = 0
    if couleur == "R":
        b = 0
        v = 0
        r = 255
    for x in range(xa - 5, xa + 5):
        for y in range(ya - 5, ya + 5):
            # print('x:',x,",y:",y)
            image[y, x] = [b, v, r]
    return image


def afficherCarre(nomimage, x1, x2, y1, y2):
    nomIm="Images/Demo/cam.jpg"
    image = afficherPoint(x1, y1, nomimage, "B")
    cv2.imwrite(nomIm, image)
    image = cv2.imread(nomIm)
    image = afficherPoint(x1, y2, nomIm, "B")
    cv2.imwrite(nomIm, image)
    image = cv2.imread(nomIm)
    image = afficherPoint(x2, y1, nomIm, "B")
    cv2.imwrite(nomIm, image)
    image = cv2.imread(nomIm)
    image = afficherPoint(x2, y2, nomIm, "B")
    cv2.imwrite(nomIm , image)
    image = cv2.imread(nomIm)
    cv2.imshow('Display2', image)
    cv2.waitKey(0)

def plot_colors(hist, centroids):
    # initialise le graphique à barres représentant la fréquence relative # de chacune des couleurs
    bar = np.zeros((50, 300, 3), dtype="uint8")
    startX = 0

    # boucle sur le pourcentage de chaque cluster et la couleur de
    # chaque groupe
    for (percent, color) in zip(hist, centroids):
        # tracer le pourcentage relatif de chaque groupe
        endX = startX + (percent * 300)
        cv2.rectangle(bar, (int(startX), 0), (int(endX), 50),
                      color.astype("uint8").tolist(), -1)
        startX = endX
    # renvoie le graphique à barres
    return bar


def centroid_histogram(clt):
    # saisir le nombre de grappes différentes et créer un histogramme
    # basé sur le nombre de pixels assignés à chaque classification
    numLabels = np.arange(0, len(np.unique(clt.labels_)) + 1)
    (hist, _) = np.histogram(clt.labels_, bins=numLabels)

    # normaliser l’histogramme de façon à ce qu’il ne fasse plus qu’un
    hist = hist.astype("float")
    hist /= hist.sum()

    # retourne à l’histogramme
    return hist