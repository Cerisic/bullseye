import sys
import SaveSQL
from PyQt5 import QtWidgets
from Menu_Principale import Ui_MainWindow

# creation de la base de donnée DartData et PlayerData si elles n'existent pas
SaveSQL.createDB()

# QtWidgets est un module importé (voir ci-dessus)
# Quand vous démarrez un script Python, il est possible de lui passer des arguments (on peut aussi parler de paramètres du script)
# sur la ligne de commande. Pour manipuler ces arguments, vous devez utiliser la liste sys.argv en ayant préalable
# charger le module sys, bien entendu. Le nom de la liste, argv, signifiant « argument values ».
#
# Les éléments de cette liste sont tous de type chaînes de caractères (classe str). Il est aussi à noter que la première
# entrée de la liste correspond au nom du script démarré. Les paramètres, à proprement parler, commencent donc à partir de l'indice 1.

# Le module Qt Widgets fournit un ensemble d'éléments d'interface utilisateur permettant de créer des interfaces utilisateur classiques de type bureau.
app = QtWidgets.QApplication(sys.argv)

# La classe QMainWindow fournit une fenêtre d'application principale.
MainWindow = QtWidgets.QMainWindow()

# Ui_MainWindow() herite de MainWindow
# ui est l'instance de la classe Ui_mainWindow provenant du fichier Menu_Principal
ui = Ui_MainWindow()

# setup ui est une fonction de la classe Ui_MainWindow()
ui.setupUi(MainWindow)
# la fenêtre MainWindow est cachée par défaut
MainWindow.show()
# signale l'intention de quitter l'interpréteur
# L'argument optionnel peut être un entier donnant l'état de sortie (par défaut zéro), ou un autre type d'objet.
# Si c'est un entier, zéro est considéré comme une "terminaison réussie" et toute valeur non nulle est considérée
# comme une "terminaison anormale" par les shells et autres.
sys.exit(app.exec_())




