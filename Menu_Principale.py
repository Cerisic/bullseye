# -*- coding: utf-8 -*-
from PyQt5 import QtCore, QtGui, QtWidgets
# Import of the second form

import SaveSQL
from Menu_Creation import Ui_formSecond
from MenuVisuGraph import Ui_FormVisuG


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(20, 20, 741, 511))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_MENU = QtWidgets.QLabel(self.centralwidget)
        self.label_MENU.setEnabled(True)
        font = QtGui.QFont()
        font.setPointSize(36)
        self.label_MENU.setFont(font)

        self.label_MENU.setObjectName("label_MENU")
        self.verticalLayout.addWidget(self.label_MENU, 0)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")


        self.btn_jouer = QtWidgets.QPushButton(self.verticalLayoutWidget)

        #self.btn_jouer.event(self.clicked_jouer())


        font = QtGui.QFont()
        font.setFamily("Segoe MDL2 Assets")
        font.setPointSize(24)
        self.btn_jouer.setFont(font)
        self.btn_jouer.setObjectName("btn_jouer")
        self.btn_jouer.clicked.connect(self.clicked_jouer)
        #self.btn_quitter.clicked.connect(self.clicked_quitter)
        #self.btn_visualiser.clicked.connect(self.clicked_visualiser)

        self.horizontalLayout.addWidget(self.btn_jouer)
        self.btn_visualiser = QtWidgets.QPushButton(self.verticalLayoutWidget)

        font = QtGui.QFont()
        font.setFamily("Segoe MDL2 Assets")
        font.setPointSize(24)
        self.btn_visualiser.setFont(font)
        self.btn_visualiser.setObjectName("btn_visualiser")

        # Si on appuie sur le bouton Visualiser, on appelle self.clicked_visuliser
        self.btn_visualiser.clicked.connect(self.clicked_visualiser)
        self.horizontalLayout.addWidget(self.btn_visualiser)
        self.btn_quitter = QtWidgets.QPushButton(self.verticalLayoutWidget)

        font = QtGui.QFont()
        font.setFamily("Segoe MDL2 Assets")
        font.setPointSize(24)
        self.btn_quitter.setFont(font)
        self.btn_quitter.setObjectName("btn_quitter")

        # Si on appuie sur le bouton quitter, on appelle self.clicked_quitter
        self.btn_quitter.clicked.connect(self.clicked_quitter)

        self.horizontalLayout.addWidget(self.btn_quitter)
        self.verticalLayout.addLayout(self.horizontalLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")

        MainWindow.setStatusBar(self.statusbar)

        # appel de la fonction de sa propore classe
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)


    # 3 procédures évenementielles qui tournent en boucle avec
    def clicked_jouer(self):
        data = SaveSQL.getAllPlayer()
        formSecond = QtWidgets.QDialog() # Un widget QDialog présente une fenêtre de premier niveau principalement utilisée pour recueillir les réponses de l'utilisateur.
        ui = Ui_formSecond() # from Menu_Creation import Ui_formSecond
        ui.setupUi(formSecond,data)
        ret = formSecond.exec_()

    def clicked_visualiser(self):
        print("click visualiser")
        data=SaveSQL.getAllPlayer()
        formVisuG = QtWidgets.QDialog()
        ui = Ui_FormVisuG()
        ui.setupUi(formVisuG, data)
        ret = formVisuG.exec_()

    def clicked_quitter(self):
        print("Fermeture programme")
        sys.exit(app.exec_())

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_MENU.setText(_translate("MainWindow", "MENU PRINCIPALE"))
        self.btn_jouer.setText(_translate("MainWindow", "Jouer"))
        self.btn_visualiser.setText(_translate("MainWindow", "Visualiser"))
        self.btn_quitter.setText(_translate("MainWindow", "Quitter"))
        MainWindow.setStyleSheet("background-color: yellow;")
        self.btn_quitter.setStyleSheet("background-color: red;")
        self.btn_jouer.setStyleSheet("background-color: green;")
        self.btn_visualiser.setStyleSheet("background-color: green;")

if __name__ == "__main__": # n'éxecute ce code que si ce document est le main
    import sys

    SaveSQL.createDB()
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    # signale l'intention de quitter l'interpréteur
    #L'argument optionnel arg peut être un entier donnant l'état de sortie (par défaut zéro), ou un autre type d'objet.
    # Si c'est un entier, zéro est considéré comme une "terminaison réussie" et toute valeur non nulle est considérée
    # comme une "terminaison anormale" par les shells et autres.
    # Enfin, nous appelons app.exec() pour lancer la boucle d'événements.
    sys.exit(app.exec_())
