import RPi.GPIO as gp
import numpy as np
import os
import cv2


def configureMultiCamera():

    gp.setwarnings(False)
# RPi.GPIO permet de se référer aux broches GPIO en utilisant soit les numéros des broches physiques sur le connecteur GPIO, soit les noms des canaux BCM du SOC Broadcom auxquels les broches sont connectées. Par exemple, la broche 24 est le canal BCM GPIO8. Pour utiliser les numéros de broches physiques de la carte, appelez
    #
    # GPIO.setmode(GPIO.BOARD)
    gp.setmode(gp.BOARD)

# Si vous avez utilisé Arduino, vous êtes probablement familier avec le fait que vous devez déclarer un "mode pin" avant
# de pouvoir l'utiliser comme entrée ou sortie. Pour définir le mode d'une broche, utilisez la fonction setup([pin], [GPIO.IN, GPIO.OUT].
# Seul les 3 premiers sont utilisés car on utilise qu'un seul adaptateur multi-cam
    gp.setup(7, gp.OUT)
    gp.setup(11, gp.OUT)
    gp.setup(12, gp.OUT)

# normalement pas besoin
    gp.setup(15, gp.OUT)
    gp.setup(16, gp.OUT)
    gp.setup(21, gp.OUT)
    gp.setup(22, gp.OUT)
    gp.setup(23, gp.OUT)
    gp.setup(24, gp.OUT)

# GPIO.output(12, GPIO.HIGH)
    #  # or
    # GPIO.output(12, 1)
    #  # or
    # GPIO.output(12, True)
# permet de désactiver les caméras 1,2,3,4 de l'adapteur 1
    gp.output(11, True)
    gp.output(12, True)
# Normalement pas besoin
    gp.output(15, True)
    gp.output(16, True)
    gp.output(21, True)
    gp.output(22, True)
    gp.output(23, True)


def enableCamera(num): # possibilité de mettre 4 caméras
    # Les caméras fonctionnent en séquentiel pas simultanément
    if num == 1:
        i2c = "i2cset -y 1 0x70 0x00 0x04"
# La méthode os.system() exécute la commande (une chaîne) dans un sous-shell. Cette méthode est implémentée en appelant
# la fonction standard C system(), et a les mêmes limitations. Si la commande génère une sortie, celle-ci est envoyée
# dans le flux de sortie standard de l'interpréteur. Lorsque cette méthode est utilisée, le shell correspondant du
# système d'exploitation est ouvert et la commande y est exécutée.
        os.system(i2c) # execute dans le shell
        print("cam1")
        cv2.waitKey(100)
        gp.output(7, False)
        gp.output(11, False)
        gp.output(12, True)
        cv2.waitKey(100)
    elif num == 2:
        i2c = "i2cset -y 1 0x70 0x00 0x05" # Liaison I2C paramétrée
        os.system(i2c)
        print("cam2")
        cv2.waitKey(100)
        gp.output(7, True)
        gp.output(11, False)
        gp.output(12, True)
        cv2.waitKey(100)
    elif num == 3:
        i2c = "i2cset -y 1 0x70 0x00 0x06"
        os.system(i2c)
        print("cam3")
        cv2.waitKey(100)
        gp.output(7, False)
        gp.output(11, True)
        gp.output(12, False)
        cv2.waitKey(100)
    elif num == 4:
        i2c = "i2cset -y 1 0x70 0x00 0x07"
        os.system(i2c)
        cv2.waitKey(100)
        print("cam4")
        gp.output(7, True)
        gp.output(11, True)
        gp.output(12, False)
        cv2.waitKey(100)


def disableMultiCamera():
    gp.output(7, False)
    gp.output(11, False) # Erreur normalement TRUE
    gp.output(12, True)

# TEST UNITAIRE :
if __name__ == "__main__":
    
    while True:
    
        # CAM1
        configureMultiCamera()
        print("configureMultiCamera")
        disableMultiCamera()
        print("disableMultiCamera")
        enableCamera(1)
        print("enableCamera 1")
        cv2.waitKey(1000)

        # Étapes pour capturer une vidéo :
        #   1)Utilisez cv2.VideoCapture() pour obtenir un objet de capture vidéo pour la caméra.
        #   2)Mettez en place une boucle infinie et utilisez la méthode read() pour lire les images en utilisant l'objet créé ci-dessus.
        #   3)Utilisez la méthode cv2.imshow() pour afficher les images dans la vidéo.
        #   4)Interrompez la boucle lorsque l'utilisateur clique sur une touche spécifique.

        cam = cv2.VideoCapture(0) # Ouvre une caméra pour la capture vidéo.
                                # l'argument 0 signifie qu'elle prend une caméra par défaut
        print("VideoCapture 1")

        while True:
            result, image = cam.read()
            if result == True:
                cv2.imshow('Cam Display', image)
                # Création d'une fenêtre GUI pour afficher une image à l'écran
                # Le premier paramètre est le titre de la fenêtre (doit être au format chaîne de caractères).
                # Le second paramètre est un tableau d'images
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        cam.release()
        cv2.destroyAllWindows()
        disableMultiCamera()
        cv2.waitKey(2000)
        print("\n")
        
        # CAM2
        configureMultiCamera()
        print("configureMultiCamera")
        disableMultiCamera()
        print("disableMultiCamera")
        enableCamera(2)
        print("enableCamera 2")
        cv2.waitKey(1000)
        cam = cv2.VideoCapture(0)
        print("VideoCapture 2")

        while True:
            result, image = cam.read()
            if result == True:
                cv2.imshow('Cam Display', image)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        cam.release()
        cv2.destroyAllWindows()
        disableMultiCamera()
        print("\n")
