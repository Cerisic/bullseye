from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_FormCamera(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(700, 700)
        self.nombre=0
        self.verticalLayoutWidget = QtWidgets.QWidget(Form)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 680, 680))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetNoConstraint)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(26)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label, 0, QtCore.Qt.AlignHCenter)
        self.label_2 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_2.setText("")
        self.label_2.setPixmap(QtGui.QPixmap("Images/AbsoluteDifference/cam1.jpg"))
        self.label_2.setScaledContents(True)
        self.label_2.setWordWrap(False)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetNoConstraint)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton_3 = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(self.click_precedent)
        self.horizontalLayout.addWidget(self.pushButton_3)
        self.pushButton_2 = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(self.click_camera)
        self.horizontalLayout.addWidget(self.pushButton_2)
        self.pushButton = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.click_suivant)
        self.horizontalLayout.addWidget(self.pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.actu_image()
        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label.setText(_translate("Form", "Menu Camera"))
        self.pushButton_3.setText(_translate("Form", "Precedent"))
        self.pushButton_2.setText(_translate("Form", "camera 1"))
        self.pushButton.setText(_translate("Form", "Suivant"))
        Form.setStyleSheet("background-color: green;")

    def click_suivant(self):
        self.nombre += 1
        if self.nombre >9:
            self.nombre = 0
        nom_dossier=def_nom_dossier(self.nombre)
        num_cam = self.get_num_cam()
        self.label_2.setPixmap(QtGui.QPixmap("Images/{}/cam{}.jpg".format(nom_dossier,num_cam)))
        print("Images/{}/cam{}.jpg".format(nom_dossier,num_cam))

    def click_precedent(self):
        self.nombre-=1
        if self.nombre<0:
            self.nombre=9
        nom_dossier = def_nom_dossier(self.nombre)
        num_cam = self.get_num_cam()
        self.label_2.setPixmap(QtGui.QPixmap("Images/{}/cam{}.jpg".format(nom_dossier, num_cam)))
        print("Images/{}/cam{}.jpg".format(nom_dossier, num_cam))

    def actu_image(self):
        print("Actualisation de l'image")
        nom_dossier = def_nom_dossier(self.nombre)
        num_cam = self.get_num_cam()
        self.label_2.setPixmap(QtGui.QPixmap("Images/{}/cam{}.jpg".format(nom_dossier, num_cam)))
        print("Images/{}/cam{}.jpg".format(nom_dossier, num_cam))

    def click_camera(self):
        print("click on camera button")
        nom=self.pushButton_2.text()
        if nom=="camera 1":
            self.pushButton_2.setText("camera 2")
        if nom=="camera 2":
            self.pushButton_2.setText("camera 1")
        print(self.pushButton_2.text())
        self.actu_image()

    def get_num_cam(self):
        nom = self.pushButton_2.text()
        num = 0
        if nom == "camera 0":
            num = 0
        if nom == "camera 1":
            num = 1
        if nom == "camera 2":
            num = 2
        if nom == "camera 3":
            num = 3
        return num



def def_nom_dossier(nombre):
    if nombre == 0:
        nom_dossier = "InitialCol"
    if nombre == 1:
        nom_dossier = "OriginalCol"
    if nombre == 2:
        nom_dossier = "DetectedDarts"
    if nombre == 3:
        nom_dossier = "AbsoluteDifference"
    if nombre == 4:
        nom_dossier = "ThresholdHard"
    if nombre == 5:
        nom_dossier = "ThresholdSweet"
    if nombre == 6:
        nom_dossier = "DartBorders"
    if nombre == 7:
        nom_dossier = "DartBorderContrast"
    if nombre == 8:
        nom_dossier = "DartBorderContrastDirection"
    if nombre == 9:
        nom_dossier = "UnwrapedImage"
    return nom_dossier

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_FormCamera()
    ui.setupUi(Form,"lol")
    Form.show()
    sys.exit(app.exec_())
