import keyboard
import Joueur
from Joueur import Joueur
import matplotlib.pyplot as plt



# VERIFIE SI UN JOUEUR EXISTE ET RETOURNE LE JOUEUR
def verifNewPlayer(nomJoueur):
    new = True
    f = open("save.txt", "r")
    lines = f.read().split(';')
    flech = []

    nbreV = 0
    for line in lines:
        # print(line)
        word = line.split('-')
        # print(word)
        # Mettre une sécurité si le word n'existe pas
        if word[0] == '':
            break
        if word[0] == nomJoueur:
            new = False
            # print("word 1 :",word[1])
            # print("joueur existant")
            nbreV = word[1]
            for i in range(2, len(word)):
                flechX = word[i].split(')')
                if flechX != '':
                    flech.append([int(flechX[0]), int(flechX[1]), int(flechX[2])])
    f.close()
    return new, nbreV, flech


# ECRIRE UN NOUVEAU JOUEUR
def writeNewPlayer(nomJoueur, nbreV, flech):
    f = open("save.txt", "a")
    text = nomJoueur + "-" + str(nbreV) + "-"
    text1 = ""
    for i in range(len(flech)):
        if i + 1 == len(flech):
            text1 = text1 + "2)" + str(flech[i][0]) + ")" + str(flech[i][1]) + ";"
        else:
            text1 = text1 + "2)" + str(flech[i][0]) + ")" + str(flech[i][1]) + "-"
    # print(text1)
    # print(len(flech1))
    textfin = text + text1
    # print(textfin)
    f.write(textfin)
    f.close()


def writeNewPlayer2(joueur):
    nomJoueur = joueur.name
    nbreV = joueur.nbreV
    flech = joueur.flech
    f = open("save.txt", "a")
    text = nomJoueur + "-" + str(nbreV) + "-"
    text1 = ""

    # Ecrire les flechettes et verifier si c'est le dernier texte pour le ";"
    for i in range(len(flech)):
        if i + 1 == len(flech):
            text1 = text1 + "2)" + str(flech[i][0]) + ")" + str(flech[i][1]) + ";"
        else:
            text1 = text1 + "2)" + str(flech[i][0]) + ")" + str(flech[i][1]) + "-"

    # print(text1)
    # print(len(flech))
    textfin = text + text1
    # print(textfin)
    f.write(textfin)
    f.close()


def saveAll(tab_joueur):
    f = open("save.txt", "r")
    lines = f.read().split(';')
    newtab_joueur = []
    # Créer une liste de joueur regroupant tous les joueurs existants
    for i in range(len(tab_joueur)):
        newtab_joueur.append(tab_joueur[i])
    # for i in range(len(newtab_joueur)):
    # print("joueur ", i)
    # print(newtab_joueur[i].name)
    # Ecrire le tableau de joueur en jeu dans le nouveau tableau
    # verifier les anciens joueurs enregistré et les rajouter au nouveau tableau
    for line in lines:
        word = line.split('-')
        # print(word[0])
        # verifier si joueur en jeu
        enJeu = False

        for i in range(len(tab_joueur)):
            if word[0] == tab_joueur[i].name:
                enJeu = True
                # print("Joueur ancien")
        if enJeu == False:
            if word[0] != '':
                # Récupérer le joueur
                # print("rentre dans enjeu false")
                new, nbreV, flech = verifNewPlayer(word[0])
                joueur = Joueur("nom", 1, 2, 3, 4)
                joueur.modif(word[0], word[1], flech)
                newtab_joueur.append(joueur)
    f.close()
    # Réécrire tous les joueurs dans le fichier.txt
    f = open("save.txt", "w")
    for i in range(len(newtab_joueur)):
        # print(newtab_joueur[i].name)
        writeNewPlayer2(newtab_joueur[i])
    f.close()
    pass


def showWho():
    f = open("save.txt", "r")
    lines = f.read().split(';')
    for line in lines:
        # print(line)
        word = line.split('-')
        # print(word)
        # Mettre une sécurité si le word n'existe pas
        if word[0] != '':
            print(word[0])
    f.close()


def showHim(name, nbreV, flech):
    print("nom :", name)
    print("nombre de victoire :", nbreV)
    print("1: Afficher le graphique du déroulement des parties (point des fléchettes jusqu'à la fin de la partie")
    print("2: Afficher le graphique des fléchettes par round")
    #LES FLECHETTES QUI N'ONT PAS TOUCHE DOIVENT ETRE QUAND MEME RENTREE avec : "numflechRound, 0 , numflechPartie"
    #car l'affichage du graphique a besoin d'avoir une suite continue de numflechPartie
    if keyboard.read_key() == "1":
        plt.title("Déroulement des parties")
        # Créer un tableau qui simule le déroulement de la partie
        flechNum = 1
        tab_point = []
        # le tableau reprendra les fléchettes dans l'ordre et doit plot dés qu'une partie est terminée
        print(flech)
        for i in range(len(flech)):
            print("flechNum : ",flechNum)
            print("flech[i][2] : ", flech[i][2])
            if flech[i][2] == flechNum:
                tab_point.append(flech[i][1])
                flechNum += 1
            else:
                print("tableau de point : ",tab_point)
                plt.plot(tab_point)
                tab_point.clear()
                tab_point.append(flech[i][1])
                flechNum = 2
            if i+1==len(flech):
                print("tableau de point : ",tab_point)
                plt.plot(tab_point)
                tab_point.clear()
                tab_point.append(flech[i][1])
        plt.xlabel('Lancé')
        plt.ylabel('Point')
        plt.show()
    if keyboard.read_key() == "2":
        # Créer un tableau qui simule le déroulement de la partie
        flechNum = 1
        tab_flech1 = []
        tab_flech2 = []
        tab_flech3 = []
        # le tableau reprendra les fléchettes dans l'ordre
        for i in range(len(flech)):
            if flech[i][0] == 1:
                tab_flech1.append(flech[i][1])
            if flech[i][0] == 2:
                tab_flech2.append(flech[i][1])
            if flech[i][0] == 3:
                tab_flech3.append(flech[i][1])
            flechNum += 1

        print(tab_flech1)
        plt.title("Les premières fléchettes par Round")
        plt.xlabel('lancé')
        plt.ylabel("points")
        barWidth=0.5

        plt.bar(range(len(tab_flech1)), tab_flech1, width=barWidth, color=['red' for i in tab_flech1])
        plt.grid(True)
        plt.show()
        plt.bar(range(len(tab_flech2)), tab_flech2, width=barWidth, color=['pink' for i in tab_flech2])
        '''
        plt.bar([tab_flech1,tab_flech2,tab_flech3], width=barWidth,height=200, color=['yellow', 'green','red'], label=['flech1', 'flech2','flech3'])
        plt.grid(True)
        plt.show()
       
        print(tab_flech2)
        plt.title("Les deuxièmes fléchettes par Round")
        plt.xlabel('points')
        plt.ylabel("lancé")
        plt.hist(tab_flech2,range=(0,len(tab_flech2)))
        plt.grid(True)
        plt.show()
        print(tab_flech3)
        plt.title("Les troisièmes fléchettes par Round")
        plt.xlabel('points')
        plt.ylabel("lancé")
        plt.hist(tab_flech3,range=(0,len(tab_flech3)))
        plt.grid(True)
        plt.show()
        '''

class save:
    pass
