from Dart import Dart
import SaveSQL

class Joueur:
    """
    name = None
    score = None
    # f1,f2 et f3 sont les flechettes attribuées au joueur.
    """
    # initialisation du joueur
    # un joueur est caractérisé par:
    #       -> un tableau flech[]
    #       -> un tableau tab_score_f[]
    #       -> un tableau tab_flech[]
    #       -> une variable String nom
    #       -> une variable int score

    def __init__(self, nom, c1, c2, c3, score,num_partie):
        self.flech=[]
        self.tab_score_f=[]
        self.tab_flech = []
        print(c1," ",c2," ",c3," de ",nom)
        self.name = nom
        self.score = score
        # association des flechettes correspondantes
        self.tab_flech.append(Dart(c1))
        self.tab_flech.append(Dart(c2))
        self.tab_flech.append(Dart(c3))
        self.num_partie_actuelle=num_partie


    def modif(self, nom, nbreV, flech):
        self.name = nom
        self.nbreV = nbreV
        self.flech = flech

    def new_game(self, score_init):
        self.score = score_init

    def new_flech(self,flech):
        # flech est un tableau sous la forme :
        # (partie,fPartie,fRound,fPoint,x,y)
        self.tab_score_f.append(flech)

    def fin_partie(self):
        for i in range(len(self.tab_score_f)):
            SaveSQL.addData(self.name,self.tab_score_f[i][0],self.tab_score_f[i][1],self.tab_score_f[i][2],self.tab_score_f[i][3],self.tab_score_f[i][4],self.tab_score_f[i][5])
        print("Tableau fléchettes de ", self.name," enregistré dans la base de donnée")

    def new_points(self, point):
        self.score = self.score - point

def create_tab_player(tab_nom,score):
    #Creer un tableau qui permet de voir quel num de flechette est déjà pris
    tab=[]
    tab_joueur=[]
    for i in range(10):
        tab.append(3*i)
    print("Le tableau des numéros à prendre : ", tab)
    # Parcourir le tab_nom pour savoir qui a déjà des flechettes attribuées et supprimer ces noms
    for i in range(len(tab_nom)):
        num=SaveSQL.verif_num_flech(tab_nom[i])
        if num!=None:
            print(tab_nom[i]," existe dans la base de donnée")
            # Recupération du numero de la partie
            num_partie=SaveSQL.get_num_partie(tab_nom[i])
            # Creation du joueur
            joueur=Joueur(tab_nom[i],num,num+1,num+2,score,num_partie)
            tab_joueur.append(joueur)
            # Suppression des numéros disponibles dans tab
            for j in range(len(tab)):
                if tab[j]==num:
                    tab[j]=2
    print("Le tableau des numéros à prendre : ",tab)
    # Repassage dans le tableau de nom pour créer les nouveaux joueurs
    for i in range(len(tab_nom)):
        num = SaveSQL.verif_num_flech(tab_nom[i])
        if num == None:
            print(tab_nom[i], " est créé pour la première fois")

            for j in range(len(tab)):
                if tab[j]!=2:
                    print("Création du joueur :", tab_nom[i])
                    joueur=Joueur(tab_nom[i],tab[j],tab[j]+1,tab[j]+2,score,1)
                    tab_joueur.append(joueur)
                    tab[j]=2
                    break
    print("Le tableau des numéros à prendre : ", tab)
    return tab_joueur

def create_tab_player2(tab_nom):
    #Creer un tableau qui permet de voir quel num de flechette est déjà pris
    tab=[]
    tab_joueur=[]
    for i in range(10):
        tab.append(3*i)
    print("Le tableau des numéros à prendre : ", tab)

    # Parcourir le tab_nom pour savoir qui a déjà des flechettes attribuées et leur donner le num de flechette attribué
    for i in range(len(tab_nom)):
        num=SaveSQL.verif_num_flech(tab_nom[i])
        if num!=None:
            print(tab_nom[i]," existe dans la base de donnée")
            # Suppression des numéros disponibles dans tab
            for j in range(len(tab)):
                if tab[j]==num:
                    tab_joueur.append((tab_nom[i],num))
                    #mettre 2 dans tab pour signifier que cette valeur est prise
                    tab[j]=2
    print("Le tableau des numéros à prendre : ",tab)
    # Enlever les numéro déja attribué à d'autre joueur pour éviter les futures problèmes de numéros similaires
    all_num=SaveSQL.get_all_numflech()
    for i in range(len(all_num)):
        for j in range(len(tab)):
            if all_num[i]==tab[j]:
                tab[j]=2
    # Repassage dans le tableau de nom pour créer les nouveaux joueurs
    for i in range(len(tab_nom)):
        num = SaveSQL.verif_num_flech(tab_nom[i])
        if num == None:
            print(tab_nom[i], " est créé pour la première fois")
            for j in range(len(tab)):
                if tab[j]!=2:

                    tab[j]=2
                    break
    print("Le tableau des numéros à prendre : ", tab)
    return tab_joueur

def get_num_flech(nom,tab_num):
    num=SaveSQL.verif_num_flech(nom) # appel de la fonction verif_num_flech de la classe SaveSQL
                                    # permet de vérifier si le nom du joueur est déjà connu dans la base de donnée SQL et renvoie son numéro de fléchette
    if num!=None:
        print(nom," existe dans la base de donnée avec le numéro de flechette ",num)
        return num,tab_num
    # Si on arrive ici,
    # Enlever les numéro déja attribué à d'autre joueur pour éviter les futures problèmes de numéros similaires
    all_num=SaveSQL.get_all_numflech() # all_num est tableau reprenant les différents nombres de fléchettes déjà utilisées auparavant par des joueurs
    for i in range(len(all_num)):
        for j in range(len(tab_num)):
            if all_num[i]==tab_num[j]:
                tab_num[j]=2
    num = SaveSQL.verif_num_flech(nom)
    if num == None:
        print(nom, " n'existe pas dans la table PlayerData")
        for j in range(len(tab_num)):
            if tab_num[j]!=2:
                num=tab_num[j]
                tab_num[j]=2 # Le numéro 2 signifie que l'identifiant de la fléchette est déjà pris par quelqu'un
                return num,tab_num
    print("Le tableau des numéros à prendre : ", tab_num)
    return 3,tab_num # en cas de défaut place 3 dans num (fléchette)