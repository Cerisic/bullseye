# -*- coding: utf-8 -*-
import cv2
from PyQt5 import QtCore, QtGui, QtWidgets
#import test
import SaveSQL
from Jeu import Ui_FormJeu
from PyQt5.QtWidgets import QMessageBox
from Joueur import * # Cette classe permet d'importer get_num_flech


class Ui_formSecond:
    def setupUi(self, formSecond, data): # data correspond aux noms des joueurs
        self.tab_num_flech = [] # initialisation d'une liste de classe
        for i in range(2,12):
            self.tab_num_flech.append(3 * i) # multiplier par 3 car chacun des joueurs a 3 fléchettes à tirer
        # on va reprendre data= les noms des joueurs
        self.dialog = formSecond

        formSecond.setObjectName("formSecond") # setObjectName permet d'associer "une clé de recherche"
        formSecond.resize(801, 634)

        # Creation de la fenêtre Qdialogue
        self.verticalLayoutWidget = QtWidgets.QWidget(formSecond) # comme dans Menu_Principal ligne 14
                                                                # La classe QWidget est la classe de base de tous les objets de l'interface utilisateur.
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(20, 10, 771, 591))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")

        # 1 er vertical Layout
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget) # La classe QVBoxLayout aligne les widgets verticalement
        # on place une box verticale dans verticalLayoutWidget
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")

        # titre Creation partie
        self.label_titre = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(24)
        self.label_titre.setFont(font)
        self.label_titre.setObjectName("label_titre")
        self.verticalLayout.addWidget(self.label_titre, 0, QtCore.Qt.AlignHCenter)

        # 1 er horizontal loyout
        self.horizontalLayout_score = QtWidgets.QHBoxLayout()
        self.horizontalLayout_score.setObjectName("horizontalLayout_score")

        # Barre d'édition score
        self.lineEdit_score = QtWidgets.QLineEdit(self.verticalLayoutWidget) # Le widget QLineEdit est un éditeur de texte à une ligne.
        self.lineEdit_score.setMaximumSize(QtCore.QSize(714, 16777215))
        font = QtGui.QFont()
        font.setPointSize(26)
        self.lineEdit_score.setFont(font)
        self.lineEdit_score.setObjectName("lineEdit_score")
        self.horizontalLayout_score.addWidget(self.lineEdit_score)

        # Label "score"
        self.label_score = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(36)
        self.label_score.setFont(font)
        self.label_score.setObjectName("label_score")
        self.horizontalLayout_score.addWidget(self.label_score)

        # Ajout de l' horizontal box à la verticale box
        self.verticalLayout.addLayout(self.horizontalLayout_score)


        self.horizontalLayout_joueur = QtWidgets.QHBoxLayout()
        self.horizontalLayout_joueur.setObjectName("horizontalLayout_joueur")

        self.lineEdit_newName = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(16)
        self.lineEdit_newName.setFont(font)
        self.lineEdit_newName.setObjectName("lineEdit_newName")
        self.lineEdit_newName.setMaxLength(10)
        self.horizontalLayout_joueur.addWidget(self.lineEdit_newName)

        self.pushButton_ajouter = QtWidgets.QPushButton(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(20)
        self.pushButton_ajouter.setFont(font)
        self.pushButton_ajouter.setObjectName("pushButton_ajouter")
        # Ajout procédure évenementielle
        self.pushButton_ajouter.clicked.connect(self.clicked_ajouter)
        self.horizontalLayout_joueur.addWidget(self.pushButton_ajouter)
        self.comboBox = QtWidgets.QComboBox(self.verticalLayoutWidget)
        for i in range(len(data)):
            self.comboBox.addItems(data[i])
        font = QtGui.QFont()
        font.setPointSize(16)
        self.comboBox.setFont(font)
        self.comboBox.setObjectName("comboBox")
        self.horizontalLayout_joueur.addWidget(self.comboBox)

        self.pushButton_ajouter2 = QtWidgets.QPushButton(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(20)
        self.pushButton_ajouter2.setFont(font)
        self.pushButton_ajouter2.setObjectName("pushButton_ajouter2")
        # Ajout procédure évenementielle
        self.pushButton_ajouter2.clicked.connect(self.clicked_ajouter2)
        self.horizontalLayout_joueur.addWidget(self.pushButton_ajouter2)

        font = QtGui.QFont()
        font.setPointSize(16)
        self.listWidget = QtWidgets.QListWidget(self.verticalLayoutWidget)
        self.listWidget.setFont(font)
        self.listWidget.setObjectName("listWidget")
        self.horizontalLayout_joueur.addWidget(self.listWidget)

        font = QtGui.QFont()
        font.setPointSize(16)
        self.listWidget2 = QtWidgets.QListWidget(self.verticalLayoutWidget)
        self.listWidget2.setFont(font)
        self.listWidget2.setObjectName("listWidget2")
        self.horizontalLayout_joueur.addWidget(self.listWidget2)

        self.pushButton_retirer = QtWidgets.QPushButton(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(20)
        self.pushButton_retirer.setFont(font)
        self.pushButton_retirer.setObjectName("pushButton_retirer")
        # procédure évenementielle
        self.pushButton_retirer.clicked.connect(self.clicked_retirer)
        self.horizontalLayout_joueur.addWidget(self.pushButton_retirer)

        self.pushButton_change = QtWidgets.QPushButton(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(20)
        self.pushButton_change.setFont(font)
        self.pushButton_change.setObjectName("pushButton_change")
        # procédure évenementielle
        self.pushButton_change.clicked.connect(self.clicked_change)
        self.horizontalLayout_joueur.addWidget(self.pushButton_change)

        # Ajout du Layout horizontal au Layout vertical
        self.verticalLayout.addLayout(self.horizontalLayout_joueur)

        self.pushButton_valider = QtWidgets.QPushButton(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(36)
        self.pushButton_valider.setFont(font)
        self.pushButton_valider.setObjectName("pushButton_valider")
        self.pushButton_valider.clicked.connect(self.clicked_valider)
        self.verticalLayout.addWidget(self.pushButton_valider)
        self.retranslateUi(formSecond)
        QtCore.QMetaObject.connectSlotsByName(formSecond)

    def retranslateUi(self, formSecond):
        # par défaut les différents widgets ont le texte form (ne pas confondre name et text comme dans VB)
        _translate = QtCore.QCoreApplication.translate
        formSecond.setWindowTitle(_translate("formSecond", "MainWindow"))
        self.label_titre.setText(_translate("Form", "CREATION PARTIE"))
        self.lineEdit_score.setText(_translate("Form", "301"))
        self.label_score.setText(_translate("Form", "score"))
        self.lineEdit_newName.setPlaceholderText("New Player")
        self.pushButton_ajouter.setText(_translate("Form", "Ajouter"))
        self.pushButton_ajouter2.setText(_translate("Form", "Ajouter"))
        self.pushButton_retirer.setText(_translate("Form", "Retirer"))
        self.pushButton_change.setText(_translate("Form", "Num+"))
        self.pushButton_valider.setText(_translate("Form", "Valider"))
        formSecond.setStyleSheet("background-color: yellow;")
        self.pushButton_retirer.setStyleSheet("background-color: red;")
        self.pushButton_valider.setStyleSheet("background-color: green;")
        self.pushButton_ajouter.setStyleSheet("background-color: blue;")
        self.pushButton_ajouter2.setStyleSheet("background-color: blue;")

    def show_popup_error(self, message):
        msg = QMessageBox()
        msg.setWindowTitle("ERREUR")
        msg.setText(message)
        msg.setIcon(QMessageBox.Critical)
        x = msg.exec_()
        pass

    def clicked_ajouter(self):
        print("click ajouter")
        nom = self.lineEdit_newName.text()
        if nom != "":
            num, self.tab_num_flech = get_num_flech(nom, self.tab_num_flech) # méthode importer de la classe joueur
            print("le tableau des numéros de flechettes: ", self.tab_num_flech)
            self.listWidget.addItem(nom)
            self.listWidget2.addItem(str(num))
            self.lineEdit_newName.setText("")
        else:
            print("Error aucun nom rentré ")
        

    def clicked_ajouter2(self):
        print("click ajouter")
        nom = self.comboBox.currentText()
        num, self.tab_num_flech = get_num_flech(nom, self.tab_num_flech)
        print("le tableau des numéros de flechettes: ", self.tab_num_flech)
        print("nom : ", nom, "     num de flechette : ", num)
        self.listWidget.addItem(nom)
        self.listWidget2.addItem(str(num))

    def clicked_retirer(self):
        print("click retirer")
        clicked_item = self.listWidget.currentRow() # renvoie un entier
        clicked_item = self.listWidget2.currentRow()
        self.listWidget.takeItem(clicked_item) # Supprime et retourne l'élément de la ligne donnée dans le widget de liste ; sinon, retourne nullptr.
        #Les éléments supprimés d'un widget de liste ne seront pas gérés par Qt, et devront être supprimés manuellement.
        self.listWidget2.takeItem(clicked_item)

    def clicked_change(self):
        try:
            print("click retirer")
            clicked_item = self.listWidget2.currentRow()
            num = int(self.listWidget2.item(clicked_item).text())
            if num + 3 < 50:
                self.listWidget2.item(clicked_item).setText(str(num + 3))
            else:
                self.listWidget2.item(clicked_item).setText(str(0))
            print(num)
        except:
            print("ERREUR Click Change")
            self.show_popup_error("Aucun joueur selectionné")

    def clicked_retirerAll(self): # appelée nulle part
        self.listWidget.clear()

    def clicked_valider(self):
        print("click valider")
        ok = True
        score_partie = 404
        print("score text=", self.lineEdit_score.text())
        try:
            print("score text int=", int(self.lineEdit_score.text()))
            score_partie = int(self.lineEdit_score.text())
            print("Le score initial est de ", score_partie)
        except: # Dans le cas où on rentrerait une chaine de caractère par exemple
            print("ERREUR :Valeur score rentrée incorrect")
            ok = False
            self.show_popup_error("ERREUR :Valeur score rentrée incorrect")
        count = self.listWidget.count() # compte le nombre de ligne qui compose la liste
        print("count=", count)
        if count == 0:
            print("ERREUR : Aucun joueur dans la partie")
            self.show_popup_error("ERREUR : Aucun joueur dans la partie")
            ok = False

        tab_Joueur = []
        new = True
        SaveSQL.getAllPlayer()
        for i in range(count):
            name = self.listWidget.item(i).text()
            numero = int(self.listWidget2.item(i).text())
            for j in range(len(tab_Joueur)): # on vérifie si il n'y a pas 2 fois le même nom ou le numéro de fléchette dans la listWidget
                if tab_Joueur[j][0] == name:
                    new = False
                    print("problème avec le nom :", name)
                    break
                if tab_Joueur[j][1] == numero:
                    new = False
                    print("problème avec le numero :", numero)
                    break
            if new == True:
                tab_Joueur.append((name, numero))
                print("name :", name, " num :", numero)
        if new == False: # Dans le cas ou il avait 2 noms ou fléchettes identiques
            self.show_popup_error("ERREUR : deux noms ou deux numéros similaires")
            tab_Joueur.clear()
        if ok == True and new == True:
            print("Creation de la base de donnee")
            SaveSQL.createDB()
            print("Creation des nouvaux joueurs dans la table PlayerData")
            data_all_player = SaveSQL.getAllPlayer()
            print("data all player :",data_all_player)
            #Verification des nouveaux joueurs
            for i in range(len(tab_Joueur)):
                nouveau_Joueur = True

                for j in range(len(data_all_player)):
                    if data_all_player[j][0]==tab_Joueur[i][0]:
                        nouveau_Joueur=False
                        print(tab_Joueur[i]," existe déjà dans la BD")

                if nouveau_Joueur==True:
                    print("Creation de ",tab_Joueur[i])
                    SaveSQL.addJoueur_Joueur(tab_Joueur[i][0],tab_Joueur[i][1]) # tab_Joueur[i][0] correspond au nom du joueur
                                                                                # tab_Joueur[i][1] correspond au  uméro de fléchette
                    SaveSQL.addData(tab_Joueur[i][0],0,0,0,0,0,0) # Cree dans DartData de la base de donnée la manche du joueur et initialise tout à 0
                    print("Ajout de ",tab_Joueur[i][0]," avec num de flechette ",tab_Joueur[i][1], " dans la BD")
            print("Creation des joueurs object")

            tab_joueur_obj=[]
            for i in range(len(tab_Joueur)):
                num_partie=SaveSQL.get_num_partie(tab_Joueur[i][0]) # on récupère le nombre de fois que le joueur a déjà joué
                joueur=Joueur( tab_Joueur[i][0], int(tab_Joueur[i][1]), int(tab_Joueur[i][1])+1, int(tab_Joueur[i][1])+2, score_partie,num_partie) # appel du constructeur de la classe Joueur
                # A chaque joueur lui est associé son nom, sa fléchette 1, sa fléchette 2, sa fléchette 3, son score et son numéro de partie
                tab_joueur_obj.append(joueur) # on ajoute ce joueur au tableau
            
            for i in range(len(tab_joueur_obj)):
                    for j in range(3):
                        print("Fleche Joueur : " ,tab_joueur_obj[i].tab_flech[j].num, "name : " , tab_joueur_obj[i].name) #tab_joueur_obj[i].name permet d'accéder à la variable de classe Joueur



                        
            self.dialog.reject() # Masque la boîte de dialogue modale
            # ouverture de la fenêtre graphique implémentant le jeu
            formJeu = QtWidgets.QDialog()
            ui = Ui_FormJeu()
            ui.setupUi(formJeu, tab_joueur_obj)
            ret = formJeu.exec_()


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    formSecond = QtWidgets.QMainWindow()
    ui = Ui_formSecond()
    ui.setupUi(formSecond)
    formSecond.show()
    sys.exit(app.exec_())