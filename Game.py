import cv2
import cv2.aruco as aruco
import numpy as np
import Camera
import Calcul
import ImageAnalysis
import MultiCam
from scipy import ndimage
from PIL.Image import Image
from PIL import Image
import Jeu


def dartsDetection(camNum, allFlechVu, engage, arucoRef):
    # camNum est le numéro de la caméra en fonctionnement (vaut 1 ou 2)
    # Au premier appel, allFlechVu = []
    # Au premier appel, engage = False
    # arucoRef = [1,2,3,4] # les 4 identifiants des marqueurs de référence

    newf = 404
    tabCamNum = []
    flechVu0 = []
    flechVu1 = []
    initialFlechVu = [] # fléchette déja vue
    finRound0 = False
    newflechette1 = 404
    markerCorners = None


    for i in range(len(allFlechVu)): # ne passe pas ici au premier appel
        flechVu0.append(allFlechVu[i])
        flechVu1.append(allFlechVu[i])
        initialFlechVu.append(allFlechVu[i])
        
    initialCamNum = camNum
    MultiCam.enableCamera(camNum) # permet d'activer la caméra considérée
    cap = cv2.VideoCapture(0)

    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
    arucoParameters = aruco.DetectorParameters_create()

    ret0, frame0 = cap.read()
    cap.release()
    gray0 = cv2.cvtColor(frame0, cv2.COLOR_BGR2GRAY)
    markerCorners0, markerIds0, rejectedImgPoints0 = aruco.detectMarkers(gray0, aruco_dict, parameters=arucoParameters)
    if np.all(markerIds0 is None): markerIds0 = []
    flechVu0, engage, finRound0, newflechette0 = Camera.checkNewMarker(markerIds0, initialFlechVu, engage, arucoRef)
    # La fonction checkNewMarker permet d'analyser l'identifiant du nouveau marqueur
    # flechVu0 est un tableau qui reprend l'identifiant des fléchettes déjà identifiée
    # engage
    # finRound0 peut être mis à jour pour plannifier la fin du round
    # newFlechette0 peut valoir 404 dans ce cas, aucune fléchette n'a été détectée sinon il vaut l'identifiant de la fléchette qui vient et qui n'a pas encore été identifiée



    # Si une nouvelle fléchette est détéctée par la cam
    if newflechette0 != 404: # 404 aucune fléchette détectée
        print("Une nouvelle fléchette a été détéctée")
        print("allFlechVu : ", allFlechVu)
        if initialCamNum == 1: # détecter la caméra
            camNum = 2
        else:
            camNum = 1

        MultiCam.enableCamera(camNum)
        cap = cv2.VideoCapture(0)
        # on prend une capture avec l'autre caméra
        ret, frame = cap.read()
        cap.release()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        markerCorners, markerIds, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=arucoParameters)

        # passage automatique d'un round à l'autre
        if np.all(markerIds is None): markerIds = []
        flechVu1, engage, finRound1, newflechette1 = Camera.checkNewMarker(markerIds, allFlechVu, engage, arucoRef) # voir fichier camera


    newTab_flechVu = []
    newTab_flechVu = Calcul.compaFlechVu(newTab_flechVu, flechVu0) # système qui ressort la nouvelle fléchette
    newTab_flechVu = Calcul.compaFlechVu(newTab_flechVu, flechVu1)
    # Ces 2 fonctions permettent de recentrer dans un tableau les fléchettes déjà identifiées
    
    allFlechVu = newTab_flechVu
    
    if initialCamNum == 1:
        newf, tabCamNum = Calcul.newflechetteCompa(newflechette0, newflechette1, newflechette1, newflechette1)
    if initialCamNum == 2:
        newf, tabCamNum = Calcul.newflechetteCompa(newflechette1, newflechette0, newflechette1, newflechette1)
    
    return frame0, newf, tabCamNum, allFlechVu, engage, finRound0
# frame0 corrrespond à l'image prise par la caméra numCam comprenant ou non la fléchette
# newf correspond à la fléchette nouvellement identifiée par cam1 et cam2
# tabCamNum correspond à un tableau qui identifie la caméra...
# allFlecheVu correspond à l'ensemble des fléchettes qui ont été identifiées
# engage correspond
# finRound = True signifie que l'on a retiré l'ensemble des fléchettes et qu'il ne reste plus que les marqueurs de référence de la cible


def scoreDetect(newf, tabCamNum, table1, model1, table2, model2):
    # Calcul du point d'impact et détermination du nombre de points marqués
    # tabCamNum = [1,2]
    Kmeans_tab = [[table1, model1], [table2, model2]]

    originalCol = [[]] * len(tabCamNum)
    frames = [[]] * len(tabCamNum)
    unwarped_images = [[]] * len(tabCamNum)

    Ximpact = [[]] * len(tabCamNum)
    Yimpact = [[]] * len(tabCamNum)
    # allImpactPoints = []

    Xfinal = [[]] * len(tabCamNum)
    Yfinal = [[]] * len(tabCamNum)

    longueur = [[]] * len(tabCamNum)
    angle = [[]] * len(tabCamNum)
    color = [[]] * len(tabCamNum)
    point = [[]] * len(tabCamNum)

    for i in range(len(tabCamNum)):
        frames[i] = Camera.localisation(tabCamNum[i], newf) # si la fléchette est détectée plus de 3x
        frames[i] = ndimage.rotate(frames[i],90)
        cv2.imwrite("Images/DetectedDarts/cam%s.jpg" % tabCamNum[i], frames[i])

    # Calcul des points d'impacts selon chaque caméra
    for i in range(len(tabCamNum)):
        originalCol[i] = cv2.imread("Images/OriginalCol/cam%s.jpg" % tabCamNum[i])
        print("originalCol Lu")
        Ximpact[i], Yimpact[i] = ImageAnalysis.substract_PcaMehtod_ImpactPoint(originalCol[i], frames[i], newf, tabCamNum[i])
        # Il s'agit des coordonnées du point de vue des caméras

    # Calcul des points d'impacts sur les images redréssées
    # pour chaque caméra
    for i in range(len(tabCamNum)):
        # Redressement de l'image et relocalisation du point d'impact
        bullsEye = cv2.imread("Images/InitialCol/cam%s.jpg" % tabCamNum[i])
        corners = ImageAnalysis.ArucoCornersDetermination(bullsEye) # Cette méthode permet d'obtenir les coins des Arucos
        unwarped_images[i] = ImageAnalysis.warpPerspective(bullsEye, corners, 0, 480, 640, Ximpact[i], Yimpact[i])
        Xfinal[i], Yfinal[i] = ImageAnalysis.detectionImpactPointUnwarped(unwarped_images[i])
        # Xfinal correspond au point d'impact sur l'image redressée
        # Yfinal correspond au point d'impact sur l'image redressée
        cv2.imwrite("Images/UnwrapedImage/cam%s.jpg" % tabCamNum[i], unwarped_images[i])
        # allImpactPoints.append([Xfinal[i], Yfinal[i]])

        # Calcul du centre de la cible
        nomimage = "Images/UnwrapedImage/Init_cam%s.jpg" % tabCamNum[i]
        xc, yc = get_center(nomimage)
        # xc et yc sont les coordonnées du centre (milieu de la hauteur et de la largeur de l'image) de la cible

        # Calcul det l'ang Calcul.le couleur lié au point d'impact et de l'amplitude formé entre le point d'impact et le centre de la cible
        longueur[i], angle[i] = Calcul.findAmpAng(xc, yc, Xfinal[i], Yfinal[i], nomimage)

        # Trouver la couleur du point d'impact
        nomimage = "Images/UnwrapedImage/CibleNet%s.jpg" % tabCamNum[i]
        print("TEST : ", Kmeans_tab[i][0])
        color[i] = ImageAnalysis.findColorPoint(int(Xfinal[i]), int(Yfinal[i]), nomimage, Kmeans_tab[i][0], Kmeans_tab[i][1])
        # retourne la couleur dans laquelle se trouve la fléchette
        print("Couleur au point d'impact Cam%s :" % (i+1), color[i])

        # Calcul des points
        point[i] = Calcul.getPoint(longueur[i], angle[i], color[i])
        print("Point Cam%s : " % (i+1), point[i])

    """
    # Triage des différents points d'impact
    impactCases, impactCases_moy = Calcul.SortList(allImpactPoints)
    XimpactPoint, YimpactPoint = Calcul.ChooseVector(impactCases, impactCases_moy)
    print("XimpactPoint : ", XimpactPoint, " et YimpactPoint : ", YimpactPoint)
    """
    return point[0] # point[0] est le score obtenu suite au lancement de la fléchette


def get_center(nomimage): # nomimage est le nom du fichier dans lequel se trouve l'image
    print("Reperage Centre : ")
    im = Image.open(nomimage)
    image = cv2.imread(nomimage)
    hauteur = np.size(im, 0) # indique la taille d'une matrice (=image) selon une direction
    # la direction 0 correspond à la vertica
    largeur = np.size(im, 1)
    # la direction 1 correspond à l'horizontal

    cv2.circle(image, (int(largeur / 2), int(hauteur / 2)), 35, (255, 0, 0), 1)
    cv2.circle(image, (int(largeur / 2), int(hauteur / 2)), 2, (0, 0, 255), -1) # représente le centre de la cible
    cv2.imwrite("Images/UnwrapedImage/ImageRedresseeAvecCentre.jpg", image)
    return int(largeur/2), int(hauteur/2) # renvoie le centre de l'image (supposé le centre de la cible)


def setpoint(newflechette, point, tab_joueur):
    # tab_joueur est un tableau de d'objet de la classe Joueur
    # Chaque joueur a une fléchette, un tableau de score, tableau de fléchette, un nom, un score,
    # Le tableau de score est rempli de 3 fléchettes de la classe Dart
    dernierTour = False
    # Parcourir le tableau de joueur pour retrouver à qui est la fléchette et lui donner les points
    for i in range(len(tab_joueur)):
        for j in range(3): # [0 1 2] pour 3 lancers
            if newflechette == tab_joueur[i].tab_flech[j].num: # si l'identifiant de la fléchette correspond bien à celui qui a été attribué pour la personne
                
                if tab_joueur[i].score - point < 0: # Si le nombre de point obtenu lors du lancer est supérieur au nombre de points qu'il reste à marquer, on réinitialise
                    # Rajouter les points des fléchettes déjà lancées pour annuler le tour
                    for k in range(3):
                        tab_joueur[i].score = tab_joueur[i].score + tab_joueur[i].tab_flech[k].score # on réaugmente le score qui avait été déduit de toutes le score des fléchettes réalisés précédemment
                    print(" Raté, le score de ", tab_joueur[i].name, " est de ", tab_joueur[i].score)

                if tab_joueur[i].score - point > 0:
                    tab_joueur[i].tab_flech[j].score = point
                    tab_joueur[i].score = tab_joueur[i].score - point
                    print(tab_joueur[i].name, " marque ", point, " et a donc un score de ", tab_joueur[i].score)

                if tab_joueur[i].score - point == 0:
                    tab_joueur[i].tab_flech[j].score = point
                    tab_joueur[i].score = tab_joueur[i].score - point
                    print(" ATTENTION, le futur gagnant est probablement ", tab_joueur[i].name)
                    dernierTour = True
    return dernierTour


def finPartie(tab_joueur):
    tab_winner = []
    for joueur in tab_joueur:
        if joueur.score == 0:
            tab_winner.append(joueur)
    return tab_winner
