from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import argparse
import utils
import cv2
import numpy as np
from numpy import linalg as LA
import cv2
import cv2.aruco as aruco
import math
import keyboard
from PIL.Image import Image
from PIL import Image, ImageDraw
import os
from sympy import symbols, simplify, sin, cos, Eq, linsolve, solve
from scipy import ndimage
import Demo
import Calcul
import Game


def substract_PcaMehtod_ImpactPoint(originalCol, afterCol, newf, camNum):
    # OrgiginalCol est ici une image sans la fléchette
    # aftergray correspond à l'image avec la fléchette
    print("Rentré dans la fonction substract")
    originalGray = cv2.cvtColor(originalCol, cv2.COLOR_BGR2GRAY)
    afterGray = cv2.cvtColor(afterCol, cv2.COLOR_BGR2GRAY)
    original_blur = cv2.GaussianBlur(originalGray, (5, 5), 0) # permet un floutage d'image
    # filtre gaussien de taille 5 x 5 et d'écart-type 0.
    after_blur = cv2.GaussianBlur(afterGray, (5, 5), 0)

    difference_abs = cv2.absdiff(original_blur, after_blur) # identifie les pixels qui ont été modifié entre la première image et la seconde image
    _, difference = cv2.threshold(difference_abs,15, 255, cv2.THRESH_BINARY)
    # Le seuillage est une technique d'OpenCV, qui consiste à affecter les valeurs des pixels en fonction de la valeur
    # seuil fournie. Dans le seuillage, chaque valeur de pixel est comparée à la valeur seuil. Si la valeur du pixel est
    # inférieure au seuil, elle est fixée à 0, sinon, elle est fixée à une valeur maximale (généralement 255).
    # Le seuillage est une technique de segmentation très populaire, utilisée pour séparer un objet considéré comme un
    # premier plan de son arrière-plan. Un seuil est une valeur qui a deux régions de part et d'autre, c'est-à-dire en
    # dessous ou au-dessus du seuil.
    # cv2.THRESH_BINARY : Si l'intensité du pixel est supérieure au seuil fixé, la valeur est fixée à 255, sinon elle
    # est fixée à 0 (noir).

    # v2.threshold(source, thresholdValue, maxVal, thresholdingTechnique)
    # Paramètres :
    # -> source : Tableau d'images d'entrée (doit être en niveaux de gris).
    # -> thresholdValue : Valeur du seuil en dessous et au-dessus de laquelle les valeurs des pixels seront modifiées en conséquence.
    # -> maxVal : Valeur maximale qui peut être attribuée à un pixel.
    # -> thresholdingTechnique : Le type de seuillage à appliquer.

    difference_blur = cv2.GaussianBlur(difference, (5, 5), cv2.BORDER_DEFAULT)
    _, difference_blur_thresholdHard = cv2.threshold(difference_blur, 150, 255, cv2.THRESH_BINARY)
    _, difference_blur_thresholdSweet = cv2.threshold(difference_blur, 85, 255, cv2.THRESH_BINARY)
    cv2.imwrite("Images/AbsoluteDifference/cam%s.jpg" % camNum, difference_abs)
    cv2.imwrite("Images/ThresholdHard/cam%s.jpg" % camNum, difference_blur_thresholdHard)
    cv2.imwrite("Images/ThresholdSweet/cam%s.jpg" % camNum, difference_blur_thresholdSweet)
    image = difference_blur_thresholdHard

    cv2.destroyAllWindows()

    x = []
    y = []
    datasetPCA = []
    hauteur = np.size(image, 0) # refere sur le nombre de pixel qui compose l'image selon l'axe 0 (donc vertical)
    largeur = np.size(image, 1) # refere sur le nombre de pixel qui compose l'image selon l'axe 1 (donc horizontal)

    for i in range(largeur): # on parcourt tous les pixels de l'image
        for j in range(hauteur):
            pixel = image[j, i]

            if pixel == 255: # s'ils sont blanc
                x.append(i) # on récupère leur coordonnée selon x et selon y
                y.append(j)

    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
    arucoParameters = aruco.DetectorParameters_create()

    gray = cv2.cvtColor(afterCol, cv2.COLOR_BGR2GRAY) # correspond à l'image avec la fléchette
    markerCorners, markerIds, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=arucoParameters)
    
    
    flechetteCorners = []
    for i in range(len(markerIds)):
        if markerIds[i][0] == newf:
            print("flechette marker detected again")
            flechetteCorners.append([int(markerCorners[i][0][0][0]), int(markerCorners[i][0][0][1])])
            flechetteCorners.append([int(markerCorners[i][0][1][0]), int(markerCorners[i][0][1][1])])
            flechetteCorners.append([int(markerCorners[i][0][2][0]), int(markerCorners[i][0][2][1])])
            flechetteCorners.append([int(markerCorners[i][0][3][0]), int(markerCorners[i][0][3][1])])
            break
    
    print("flechetteCorners de la nouvelle flechette détéctée pour la camera n° %s: " % camNum, flechetteCorners)
    # flecette corners est une liste qui reprend les coordonnées en x et y des 4 coins du marqueurs de la fléchette à détectée
    
    x, y, limitLineForImpactPoint, cas = flechetteMask(afterCol, flechetteCorners, x, y, camNum)
    # x sont les coordonnées en x des points qui se situent entre les droites directrices définies au dessus qui constituent la fléchette par différence entre 2 images
    # y sont les coordonnées en x des points qui se situent entre les droites directrices définies au dessus qui constituent la fléchette par différence entre 2 images

    x_mean = np.mean(x) # permet de calculer la moyenne des coordonnées en x des points blancs définies entre les lignes directrices
    y_mean = np.mean(y)
    
    print("Nombre de pixels gardés pour LA après le masque : ", len(x))
    for i in range(len(x)):
        datasetPCA.append([x[i], y[i]]) # datasetPCA est une liste qui comprent l'ensemble des coordonnées des points

    cov_mat = np.cov(datasetPCA, rowvar=False)
    # permet de mesurer la covariance de datasetPCA
    # rowvarbool, facultatif
    # Si rowvar est True (par défaut), alors chaque ligne représente une variable, avec des observations dans les colonnes.
    # Sinon, la relation est transposée : chaque colonne représente une variable, tandis que les lignes contiennent des observations.
    # La covariance est une mesure de l'association ou du lien qui existe entre deux variables.

    # Une matrice de variance/covariance est une matrice carrée qui comporte les variances et les covariances associées
    # à plusieurs variables. Les éléments de diagonale de la matrice contiennent les variances des variables, tandis que
    # les éléments hors diagonale contiennent les covariances entre toutes les paires possibles de variables.

    w, v = LA.eig(cov_mat)
    # np.linalg permet de calculer les valeurs propres et les vecteurs propres droits d'un tableau carré.

    # Une matrice carré de dimension/taille n possède n valeurs propres. Attention cependant, certaines valeurs propres peuvent être identiques, pour connaitre le nombre
    #  de valeurs propres distinctes (sans multiplicité) alors calculer les racines (distinctes) du polynome caractéristique de la matrice.
    # le concept de vecteur propre correspond à l'étude des axes privilégiés,
    print("Valeurs propres : ", w)
    print("Vecteurs propres : ", v)

    plt.figure()
    plt.scatter(x, y, alpha=0.3)
    # La valeur du mélange alpha, entre 0 (transparent) et 1 (opaque).

    # Utilisation des vecteurs propres et valeurs moyennes pour tracer une droite  y = m . x + p
    m = v[1][0] / v[1][1]
    p = y_mean - m * x_mean
    a = np.linspace(0, 640, 10000) # permet de créer un vecteur allant de 0 à 640 avec 10000 points
    b = m * a + p

    plt.scatter(a, b)
    len0 = np.sqrt(w[0]) # retourne la racine carrée de la valeur propre
    len1 = np.sqrt(w[1])

    plt.arrow(x_mean, y_mean, len0 * v[:, 0][0], len0 * v[:, 0][1], head_width=5)
    # permet de dessiner un vecteur de coordonnée de départ (x_mean,y_mean) et de longueur len0 * v[:, 0][0] selon x
    # et de longueur len0 * v[:, 0][1] selon y avec une épaisseur de 5
    plt.arrow(x_mean, y_mean, len1 * v[:, 1][0], len1 * v[:, 1][1], head_width=5)
    plt.axis('equal')
    plt.xlim(0, 640) # nombre de pixel
    plt.ylim(0, 480)
    plt.title("PCA method")
    # plt.show()

    print("m : ", m)
    print("p : ", p)

    dartDirectionParam = [m, p]
    ptImpactX, ptImpactY = impactPoint(dartDirectionParam, difference_blur_thresholdSweet, x_mean, flechetteCorners,
                                       limitLineForImpactPoint, cas, camNum)

    print('ptX : ', ptImpactX)
    print('ptY : ', ptImpactY)

    return ptImpactX, ptImpactY


def flechetteMask(image, flechetteCorners, x, y, camNum):
    # image est l'image normale comprenant la fléchette
    # fléchetteCorners est un tableau comprenant les coordonnées (x,y) de chaque coin du marqueur de a fléchette
    # x est un tableau reprenant les coordonnées en x des pixels blancs sur l'image isolant la fléchette
    # y est un tableau reprenant les coordonnées en y des pixels blancs sur l'image isolant la fléchette
    # calcul d'une distance sqrt((y0-y1)²+(x0-x1)²)
    markerImageDimension = math.sqrt(math.pow((flechetteCorners[0][1] - flechetteCorners[1][1]), 2) + math.pow(
        (flechetteCorners[0][0] - flechetteCorners[1][0]), 2))
    
    print("markerImageDimension", markerImageDimension)
    
    # C'est le grand marqueur qui a été détécté : 
    if markerImageDimension >= 17: 
        facteurEchelle = markerImageDimension / 2.3
        largeurTeteFlechette = 3.5
        longueurFlechette = 15.7
    # C'est le petit marqueur qui a été détécté : 
    else:
        facteurEchelle = markerImageDimension / 0.9
        largeurTeteFlechette = 3.5
        longueurFlechette = 15.7

    # Calcul d'une variable d'ajustement "adjust" en focntion de l'angle de vue de la caméra, ainsi peu importe
    # la position de la flecheette dans l'image, les calculs méneront toujours aux résultats souhaités
    # Vérification du sens de l'Aruco de la fléchette pour changer ou non la variable d'ajustement:

    adjust = 1
    adjustSuppl = 1
    cornersAdjust = 0
    cas = 1
    adjust = 1
    cornersAdjust = 0
    adjustSuppl = 1
    """
    if flechetteCorners[0][1] < flechetteCorners[2][1]:
        if flechetteCorners[0][0] < flechetteCorners[2][0]:
            cas = 1
            # C'est l'Aruco dit debout qui a été repéré, fléchette pointant vers la droite
            adjust = 1
            cornersAdjust = 0
            adjustSuppl = 1
        if flechetteCorners[0][0] > flechetteCorners[2][0]:
            cas = 2
            # C'est l'Aruco dit retourné qui a été repéré, fléchette pointant vers la gauche
            adjust = 1
            cornersAdjust = 2
            adjustSuppl = -1

    if flechetteCorners[0][1] > flechetteCorners[2][1]:
        if flechetteCorners[0][0] < flechetteCorners[2][0]:
            cas = 3
            # C'est l'Aruco dit retourné qui a été repéré, fléchette pointant vers la droite
            adjust = -1
            cornersAdjust = 2
            adjustSuppl = 1
        if flechetteCorners[0][0] > flechetteCorners[2][0]:
            cas = 4
            # C'est l'Aruco dit debout qui a été repéré, fléchette pointant vers la gauche
            adjust = -1
            cornersAdjust = 0
            adjustSuppl = -1
    """
    print("cas : ", cas)
    # Calcule des droites de masquage verticales et horizontales
    # Droite horizontale
    # flechetteCorners [0->3] représente le numéro du coins
    # flechetteCorners[i][0] représente la coordonnée en x du coin i
    # flechetteCorners[i][1] représente la coordonnée en y du coin i
    m1 = (flechetteCorners[0][1] - flechetteCorners[1][1]) / (
                flechetteCorners[0][0] - flechetteCorners[1][0])
    p1 = flechetteCorners[0][1] - m1 * flechetteCorners[0][0]
    # Droite horizontale translatée un peu vers le haut pour avoir un peu de marge
    pixelsDeTransla = -20
    p1 = p1 + pixelsDeTransla
    # Droite verticale
    if (flechetteCorners[0][0] - flechetteCorners[3][0]) == 0: # si le coin 0 et le coin 4 du marqueur sont confondu
        m2 = (flechetteCorners[0][1] - flechetteCorners[3][1]) / 1
    else:
        m2 = (flechetteCorners[0][1] - flechetteCorners[3][1]) / (
                    flechetteCorners[0][0] - flechetteCorners[3][0])

    p2 = flechetteCorners[0][1] - m2 * flechetteCorners[0][0]

    hauteur = np.size(image, 0)
    largeur = np.size(image, 1)

    pixelDecalage1 = largeurTeteFlechette * facteurEchelle
    margeSuppl = 1.35
    pixelDecalage1 = pixelDecalage1 * margeSuppl
    if markerImageDimension >= 23:
        pixelDecalage2 = longueurFlechette * facteurEchelle
        margeSuppl = 1.65
        pixelDecalage2 = pixelDecalage2 * margeSuppl
    else:
        pixelDecalage2 = longueurFlechette * facteurEchelle
        margeSuppl = 1.50
        pixelDecalage2 = pixelDecalage2 * margeSuppl
    
    print("pixelDecalage2", pixelDecalage2)
    
    verif2 = True
    transla2 = 0
    transla1 = 0
    x0 = 0
    for i in range(largeur):
        x0 += 1
        y1 = m1 * x0 + p1
        y2 = m2 * x0 + p2
        y1_bis = m1 * x0 + (p1 - pixelsDeTransla)
        y2_bis = m2 * (x0 - 1.2 * pixelDecalage1) + p2
        distance2 = math.sqrt(math.pow(flechetteCorners[0][1] - y1_bis, 2) + math.pow((flechetteCorners[0][0] - x0), 2))

        if (y1 < hauteur) and (y1 > 0) and (x0 < largeur):
            image[int(y1), int(x0)] = (0, 0, 255)
        if (y2_bis < hauteur) and (y2_bis > 0) and (x0 < largeur):
            image[int(y2_bis), int(x0)] = (0, 0, 255)

        if adjustSuppl == -1 and distance2 <= pixelDecalage2 and flechetteCorners[0][0] > x0 and verif2 == True:
            verif2 = False
            transla2 = abs((x0 - flechetteCorners[0][0]))
        if adjustSuppl == 1 and distance2 >= pixelDecalage2 and flechetteCorners[0][0] < x0 and verif2 == True:
            verif2 = False
            transla2 = abs((x0 - flechetteCorners[0][0]))
            transla1 = - abs((y1_bis - flechetteCorners[0][0]))

    x0 = 0

    # Définition des droites paralléles aux première
    m3 = m1
    p3 = (flechetteCorners[3][1] - m1 * flechetteCorners[3][0]) + (pixelsDeTransla * -1)
    m4 = m2
    p4 = -(m2 * transla2 * adjustSuppl) + (p2 + transla1)  # y4 = m2 * (x0 - transla2*adjustSuppl) + p2

    limitLineForImpactPoint = [m4, p4]
    print(largeur)
    for i in range(largeur):
        x0 += 1
        y3 = m3 * x0 + p3
        y4 = m4 * x0 + p4

        if (y3 < hauteur) and (y3 > 0) and (x0 < largeur):
            image[int(y3), int(x0)] = (0, 0, 255)
        if (y4 < hauteur) and (y4 > 0) and (x0 < largeur):
            image[int(y4), int(x0)] = (0, 0, 255)

    cv2.imwrite("Images/DartBorders/cam%s.jpg" % camNum, image)

    # Pour vérifier si le pixel de l'image initiale est inscrit dans les limites des droites que l'on vient d'établir il
    # faut vérifier si ce point se trouve entre les deux droites verticales et les deux droites horizontales
    xKept = []
    yKept = []

    for i in range(len(x)):

        # Verification si le y du pixel est compris entre celui des droites horizontales pour le même x
        y1 = m1 * x[i] + p1
        y2 = m3 * x[i] + p3
        # Verification si le x du pixel est compris entre celui des droites verticales pour le même y
        x1 = ((y[i] - p2) / m2) + 1.2 * pixelDecalage1
        x2 = (y[i] - p4) / m4

        if (adjust == 1) and (adjustSuppl == 1) and (y[i] > y1) and (y[i] < y2) and (x[i] > x1) and (x[i] < x2):
            xKept.append(x[i])
            yKept.append(y[i])
            image[y[i], x[i]] = (0, 0, 255)

        if (adjust == 1) and (adjustSuppl == -1) and (y[i] > y1) and (y[i] < y2) and (x[i] < x1) and (x[i] > x2):
            xKept.append(x[i])
            yKept.append(y[i])
            image[y[i], x[i]] = (0, 0, 255)

        if (adjust == -1) and (adjustSuppl == 1) and (y[i] < y1) and (y[i] > y2) and (x[i] > x1) and (x[i] < x2):
            xKept.append(x[i])
            yKept.append(y[i])
            image[y[i], x[i]] = (0, 0, 255)

        if (adjust == -1) and (adjustSuppl == -1) and (y[i] < y1) and (y[i] > y2) and (x[i] < x1) and (x[i] > x2):
            xKept.append(x[i])
            yKept.append(y[i])
            image[y[i], x[i]] = (0, 0, 255)

    cv2.imwrite("Images/DartBorderContrast/cam%s.jpg" % camNum, image)
    # xkept sont les coordonnées en x des points qui se situent entre les droites directrices définies au dessus qui constituent la fléchette par différence entre 2 images
    # ykept sont les coordonnées en x des points qui se situent entre les droites directrices définies au dessus qui constituent la fléchette par différence entre 2 images
    # limitLineForImpactPoint est une droite directrice rouge
    # cas
    return xKept, yKept, limitLineForImpactPoint, cas


def impactPoint(dartDirectionParam, image, x_mean, flechetteCorners, limitLineForImpactPoint, cas, camNum):
    # dartDirectionParam est la droite qui caractérise la direction de la fléchette
    # image est difference_blur_thresholdSweet
    # flechetteCorners est une liste qui reprend les coordonnées en x et y des 4 coins du marqueurs de la fléchette à détectée
    # limitLineForImpactPoint est une limite qui permet de caractériser les limites dans lesquels les points de la fléchette doivent se retrouver
    # cas = 1
    # camNum est un tableau égal à [1,2]
    m = dartDirectionParam[0]
    p = dartDirectionParam[1]
    m1 = limitLineForImpactPoint[0]
    p1 = limitLineForImpactPoint[1]

    if cas != 0:
        # Intersection entre la direction de la fléchette et la droite limitante
        # eq1 => y = m * x + p
        # eq2 => y1 = m1 * x + p1
        x, y = symbols('x, y') # expression symbolique x et y sont considérés comme des variables symboliques
        eq1 = m * x + p - y
        eq2 = m1 * x + p1 - y
        result = solve((eq1, eq2), (x, y)) # supposé que l'équation est sous la forme ...=0
        # exemple:
        # x, y = symbols('x, y')  # expression symbolique x et y sont considérés comme des variables symboliques
        # eq1 = 2 * x + 3 - y
        # eq2 = 4 * x + 5 - y
        # result = solve((eq1, eq2), (x, y))
        # print(result)
        # xLim = result[x]
        # yLim = result[y]
        # print("xLim : ", result[x])
        # print("yLim : ", result[y])
        # ------------------------------------------------------------------------------------------------------------
        # {x: -1, y: 1}
        # xLim :  -1
        # yLim :  1
        print(result)
        xLim = result[x]
        yLim = result[y]
        print("xLim : ", result[x])
        print("yLim : ", result[y])
        Contrast = cv2.imread("Images/DartBorderContrast/cam%s.jpg" % camNum)
        cv2.circle(Contrast, (int(xLim), int(yLim)), 3, (255, 0, 0), -1)
        # result est donc l'intersection entre la direction de la fléchette et la droite limitante

    tableX = []
    tableY = []
    ptImpactX = 0
    ptImpactY = 0
    X1 = int(x_mean)
    X2 = int(x_mean)

    # L'idée de cette boucle est de déterminer les pixels (et de les placer en couleur verte) qui sont en blancs et qui sont le long de la droite directrice de la fléchette
    while True:
        pixel1 = 0 # reprend la couleur du pixel 1
        pixel2 = 0 # reprend la couleur du pixel 2
        X1 += 1
        X2 -= 1
        Y1 = m * X1 + p # on prend des points sur la droite qui caractérise la direction de la fléchette. On démarre à partir de la moyenne des coordonnées x (x_mean)
        Y1 = int(Y1)
        Y2 = m * X2 + p
        Y2 = int(Y2)

        if (Y1 > 0) and (Y1 < np.size(image, 0)) and (Y2 > 0) and (Y2 < np.size(image, 0)):
            pixel1 = image[Y1, X1] # image est difference_blur_thresholdSweet
            pixel2 = image[Y2, X2]

        if cas == 1:
            if (pixel1 == 255) and X1 < xLim: # si le pixel est blanc et on est inférieur à la limite définie par la droite verticale
                Contrast[Y1, X1] = (0, 255, 0)
                tableX.append(X1)
                tableY.append(Y1)
            if (pixel2 == 255) and X2 < xLim:
                Contrast[Y2, X2] = (0, 255, 0)
                tableX.append(X2)
                tableY.append(Y2)

        if cas == 2:
            if (pixel1 == 255) and X1 > xLim:
                Contrast[Y1, X1] = (0, 255, 0)
                tableX.append(X1)
                tableY.append(Y1)
            if (pixel2 == 255) and X2 > xLim:
                Contrast[Y2, X2] = (0, 255, 0)
                tableX.append(X2)
                tableY.append(Y2)

        if cas == 0:
            if (pixel1 == 255) and Y1 < (flechetteCorners[0][1] + m1):
                Contrast[Y1, X1] = (0, 255, 0)
                tableX.append(X1)
                tableY.append(Y1)
            if (pixel1 == 255) and Y2 > (flechetteCorners[0][1]):
                Contrast[Y1, X1] = (0, 255, 0)
                tableX.append(X1)
                tableY.append(Y1)

        if X1 == (np.size(image, 1) - 1) or X2 == 0: # si la coordonnée en x dépasse la largeur de l'image, on sort de la boucle
            break


    cv2.imwrite("Images/DartBorderContrastDirection/cam%s.jpg" % camNum, Contrast)
    if camNum == 2:
        if cas == 1 and m >= 0:
            ptImpactX = np.max(tableX)
            ptImpactY = np.max(tableY)
        if cas == 1 and m < 0:
            ptImpactX = np.max(tableX)
            ptImpactY = np.min(tableY)
    
    if camNum == 1:
        if cas == 1 and m <= 0:
            ptImpactX = np.max(tableX)
            ptImpactY = np.min(tableY)
        if cas == 1 and m > 0:
            ptImpactX = np.max(tableX)
            ptImpactY = np.max(tableY)
    
    cv2.circle(Contrast, (ptImpactX, ptImpactY), 35, (255, 0, 0), 1)
    cv2.imwrite("Images/DartBorderContrastDirection/PtImpact_cam%s.jpg" % camNum, Contrast)
    return ptImpactX, ptImpactY


def ArucoCornersDetermination(originalCol): # originalCol est une image
    gray = cv2.cvtColor(originalCol, cv2.COLOR_BGR2GRAY)

    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
    arucoParameters = aruco.DetectorParameters_create()

    # Ne faut-il pas mettre gray à la place de originalCol
    markerCorners, markerIds, rejectedImgPoints = aruco.detectMarkers(originalCol, aruco_dict, parameters=arucoParameters)
    print("markerIds dans ArucoCornersDetermination : ", markerIds)
    corners = [[], [], [], []]

    for i in range(len(markerIds)):

        if markerIds[i][0] == 1:
            corners[0].append(markerCorners[i][0][0])

        if markerIds[i][0] == 2:
            corners[1].append(markerCorners[i][0][1])

        if markerIds[i][0] == 3:
            corners[2].append(markerCorners[i][0][3])

        if markerIds[i][0] == 4:
            corners[3].append(markerCorners[i][0][2])

    print("Corners : ", corners)

    return corners


def warpPerspective(image, tagList, offset, width, height, ptImpactX, ptImpactY):
    # https://theailearner.com/tag/cv2-getperspectivetransform/

    # tagList est la liste comprenant les coordonnées des points de référence des 4 arucos: [[][][][]]
    # FUNCTION to warp perspective of the image
    # Source : Ayoub K. and Alex K.
    # Width et height sont les dimensions des photos prises par la pi camera 640 pixels de largeur sur 480 pixels de hauteur

    if ptImpactX != 0 and ptImpactY != 0:           # (rouge,vert,bleu)
        cv2.circle(image, (ptImpactX, ptImpactY), 35, (255, 0, 0), 1) # cv2.circle(image, center_coordinates, radius, color, thickness)
        cv2.circle(image, (ptImpactX, ptImpactY), 2, (0, 0, 255), -1)
        # épaisseur : Il s'agit de l'épaisseur de la ligne de bordure du cercle en px. Une épaisseur de -1 px remplira
        # la forme du cercle avec la couleur spécifiée.

    # utilisation de 2 marqueurs aruco possibles puisqu'il s'agit d'un rectangle
                                # x                         #y
    original = np.float32([[tagList[0][0][0] - offset, tagList[0][0][1]],
                           [tagList[1][0][0] + offset, tagList[1][0][1]],
                           [tagList[2][0][0] + offset, tagList[2][0][1]],
                           [tagList[3][0][0] - offset, tagList[3][0][1]]])
    unwarped = np.float32([[0, 0], [480, 0], [0, 640], [480, 640]]) # on veut redresser toute l'image (on mentionne les 4 coins de l'image à redresser)
    matrix = cv2.getPerspectiveTransform(original, unwarped) # constitue la matrice de transformation
    unwarped_img = cv2.warpPerspective(image, matrix, (width, height))
    # applique la matrice de transformation à l'image de départ et reconstitue une image de taille width et height

    return unwarped_img

def detectionImpactPointUnwarped(unwarped_img):
    okRed = []
    hauteur = np.size(unwarped_img, 0)
    largeur = np.size(unwarped_img, 1)

    for i in range(largeur):
        for j in range(hauteur):
            pixel = unwarped_img[j, i]
            if pixel[0] == 0 and pixel[1] == 0 and pixel[2] == 255:
                okRed.append([i, j]) # permet de localiser le cercle rouge qui a été précédemment dessiné au point d'impact
                                     # (i,j) sont les coordonnées du point d'impact

    Xtotal = [] # coordonée en x du point d'impact
    Ytotal = [] # coordonée en y du point d'impact
    for i in range(len(okRed)):
        Xtotal.append(okRed[i][0])
        Ytotal.append(okRed[i][1])

    Xfinal = np.mean(Xtotal)
    Yfinal = np.mean(Ytotal)
    print("Xfinal : ", Xfinal)
    print("Yfinal : ", Yfinal)

    return Xfinal, Yfinal


def findCenter(nomimage, model,table):
    image = cv2.imread(nomimage)
    im = Image.open(nomimage)
    x1 = 250
    x2 = 400
    y1 = 250
    y2 = 400

    Demo.afficherCarre(nomimage, x1, x2, y1, y2)
    rouge_tab= findColorRed(nomimage, x1, x2, y1, y2, model,table)
    print("Tableau des pixels Rouges trouvés:",rouge_tab)
    yc = int(Calcul.moyenne(rouge_tab, 0))
    xc = int(Calcul.moyenne(rouge_tab, 1))
    Demo.afficherPoint(xc, yc, nomimage, "B")
    return xc, yc


def findColorRed(nomimage, xmin, xmax, ymin, ymax, model, table):
    im = Image.open(nomimage)
    rouge_tab = []
    rouge_tab.clear()

    for x in range(xmin, xmax):
        for y in range(ymin, ymax):
            (rouge, vert, bleu) = im.getpixel((x, y))
            couleur = model.predict([(rouge, vert, bleu)])
            nom_couleur = table[couleur[0]]
            ### ROUGE
            if nom_couleur == "rouge":
                rouge_tab.append([y, x])
    return rouge_tab


def findColorPoint(xa, ya, nomimage, table, model):
    color = None
    nbrerouge = 0
    nbrenoir = 0
    nbrevert = 0
    nbrebeige = 0
    nbrebleu = 0
    image = cv2.imread(nomimage)
    copy = cv2.imread(nomimage)
    # Il s"agit d'une copie de l'image, on ne réalise pas le cercle sur l'image originale
    cv2.circle(copy, (xa, ya), 35, (255, 0, 0), 1)
    cv2.circle(copy, (xa, ya), 2, (0, 0, 255), -1)
    cv2.imwrite("Images/UnwrapedImage/ptImpactCibleNet.jpg", copy)
    
    for x in range(xa - 5, xa + 5): # on regarde tous les points écarté de 5 pixels du centre
        for y in range(ya - 5, ya + 5):
            (bleu, vert, rouge) = image[y,x]
            centroid = model.predict([(bleu, vert, rouge)]) # La fonction predict permet de déterminer l'index du cluster le plus proche de ce triplet de valeur.

            nom_couleur = table[centroid[0]] # table est une table qui reprend les couleurs dans l'ordre des centroides

            if x == xa and y == ya:
                print("Couleur pour le pixel du pt d'impact : ", nom_couleur)
            ### ROUGE
            if nom_couleur=="rouge":
                nbrerouge += 1
                copy[y, x] = (0,0,255)
            ### VERT
            if nom_couleur=="vert":
                nbrevert += 1
                copy[y, x] = (0,255,0)
            ### BEIGE
            if nom_couleur=="beige":
                nbrebeige += 1
                copy[y, x] = (255,255,255)
            ### NOIR
            if nom_couleur=="noir":
                nbrenoir += 1
                copy[y, x] = (0,0,0)
            ### BLEUPUR
            if nom_couleur=="bleuPur":
                nbrebleu += 1
                copy[y, x] = (255,0,0)

    print("nbrerouge : ", nbrerouge )
    print("nbrenoir : ", nbrenoir)
    print("nbrebeige : ", nbrebeige)
    print("nbrevert : ", nbrevert)
    print("nbrebleu : ", nbrebleu)
    
    cv2.imwrite("Images/UnwrapedImage/ptImpactCouleursKmeans.jpg", copy)
    if nbrerouge > nbrebeige and nbrerouge > nbrenoir and nbrerouge > nbrevert and nbrerouge > nbrebleu:
        color = "rouge"
    if nbrebeige > nbrerouge and nbrebeige > nbrenoir and nbrebeige > nbrevert and nbrebeige > nbrebleu:
        color = "beige"
    if nbrenoir > nbrebeige and nbrenoir > nbrerouge and nbrenoir > nbrevert and nbrenoir > nbrebleu:
        color = "noir"
    if nbrevert > nbrebeige and nbrevert > nbrenoir and nbrevert > nbrerouge and nbrevert > nbrebleu:
        color = "vert"
    if nbrebleu > nbrebeige and nbrebleu > nbrenoir and nbrebleu > nbrerouge and nbrebleu > nbrevert:
        color = "bleuPur"
    return color


def findColor(nomimage, nbrCluster):
    color = "rien"
    color_table = [[]] * nbrCluster
    vu = []
    
    Xcenter, Ycenter = Game.get_center(nomimage) # renvoie le centre de l'image (supposé le centre de la cible)
                                                 # enregistre également une image REDRESSEE de la cible avec les 4 marqueurs de référence ainsi que le centre de la cible
    # charge l’image qui sera en format BGR
    image = cv2.imread(nomimage)
    
    # Limitation de l'image à une zone elipse utile
    cv2.ellipse(image, (Xcenter, Ycenter), (272, 345), 0, 0, 360, (255, 0, 0), 175)
    # cv2.ellipse(image, centerCoordinates, axesLength, angle, startAngle, endAngle, color [, thickness[, lineType[, shift]]])
    cv2.imwrite("Images/UnwrapedImage/EllipseLimits.jpg", image)

    image = image.reshape((image.shape[0] * image.shape[1], 3)) # [[238 247 251]
                                                                # [238 247 251]
                                                                # ...
                                                                # [238 247 251]
                                                                # [238 247 251]
                                                                # [238 247 251]
                                                                # [242 251 255]
                                                                # [236 245 249]
                                                                # [235 244 248]]
    # remodèle l'image pour qu'elle soit une liste de pixels et non une matrice
    # image.shape() fournit les dimensions de l'image hauteur/largeur
    # La forme d'une image est accessible par img.shape. Il retourne un tuple du nombre de lignes, de colonnes et de canaux (si l'image est en couleur).
    # image.shape[0] correspond à la hauteur
    #image.shape[1] correspond à la largeur
    # La multiplication des 2 indique le nombre de pixels dans l'image

    # classer les intensités de couleur de pixels avec KMeans
    print("Debut Kmeans")
    model = KMeans(n_clusters=nbrCluster) # il y a 5 clusters car il y a 5 couleurs dominantes sur la cible: le rouge,le vert, le noir, le beige
    model.fit(image) # permet de grouper les pixels en fonction de leur couleur
    rougep_tab = []
    vertp_tab = []
    bleup_tab = []
    somme_tab = []

    # Association des couleurs au centroids
    centroid_table = model.cluster_centers_
    # Dans ce cas-ci, centroid_table est de la forme [[B1,G1,R1],[B2,R2,R2],[B3,R3,R3],[B4,R4,R4],[B5,R5,R5]]
    for i in range(len(centroid_table)): # 5 centroides
        print("Centroid %s : " % i, centroid_table[i])
        somme = centroid_table[i][0] + centroid_table[i][1] + centroid_table[i][2]
        somme_tab.append(somme)
        if somme > 0:

            bleup = (centroid_table[i][0] / somme) * 100 # proportion de bleu dans le centroide considéré
            vertp = (centroid_table[i][1] / somme) * 100 # proportion de vert dans le centroide considéré
            rougep = (centroid_table[i][2] / somme) * 100 # proportion de rouge dans le centroide considéré
            # print("bleup :",bleup,",rougep : ",rougep,",vertp :",vertp)
        else: # noir pur (0,0,0) -> somme = 0 -> division par 0 au dessus
            bleup = 0
            rougep = 0
            vertp = 0
        rougep_tab.append(rougep) # ce tableau reprend la proportion en % de la couleur rouge de chaque centroide
        vertp_tab.append(vertp)
        bleup_tab.append(bleup)
        # print("rougep :", rougep, "vertp : ", vertp, "bleup : ", bleup)
    lebleuPur = 15
    lenoir = 15
    levert = 15
    lebeige = 15
    lerouge = 15
    
    # Association couleur BLEUPUR
    maxbleu = np.max(bleup_tab) # reprend la proportion maximum en % de la couleur bleu parmi tous les centroides
    for j in range(len(bleup_tab)):
        if maxbleu == bleup_tab[j]: # recherche l'index
            color_table[j] = 'bleuPur'
            lebleuPur = j # index
            rougep_tab[j] = 0
            vertp_tab[j] = 0
            bleup_tab[j] = 0
            break

    # Association couleur NOIR
    minsomme = np.min(somme_tab) # la ou la somme des couleurs est minimum, la couleur noire a le plus de chance de s'y trouver
    for j in range(len(somme_tab)):
        if minsomme == somme_tab[j] and j != lebleuPur:
            color_table[j] = 'noir'
            lenoir = j # index du centroide ou il peut y avoir du noir
            rougep_tab[j] = 0
            vertp_tab[j] = 0
            break

    # Association couleur BEIGE # la couleur beige est la couleur la plus proche du blanc
    maxsomme = np.max(somme_tab)
    for j in range(len(somme_tab)):
        if maxsomme == somme_tab[j] and j != lebleuPur and j != lenoir:
            color_table[j] = 'beige'
            lebeige = j
            rougep_tab[j] = 0
            vertp_tab[j] = 0

    # Association couleur ROUGE
    maxrouge = np.max(rougep_tab)
    for j in range(len(rougep_tab)):
        if maxrouge == rougep_tab[j] and j != lebleuPur and j != lenoir and j != lebeige:
            lerouge = j
            color_table[j] = 'rouge'
            rougep_tab[j] = 0
            vertp_tab[j] = 0

    # Association couleur VERTE
    maxvert = np.max(vertp_tab)
    for j in range(len(vertp_tab)):
        if maxvert == vertp_tab[j] and j != lebleuPur and j != lenoir and j != lebeige and j != lerouge:
            color_table[j] = 'vert'
    
    print("color_table : ", color_table)
    # color_table est une table qui reprend les couleurs dans l'ordre des centroides
    return color_table, model


if __name__ == "__main__":

    cibleNet = cv2.imread("Images/UnwrapedImage/photo.jpg")
    corners = ArucoCornersDetermination(cibleNet)
    cibleNet = warpPerspective(cibleNet, corners, 0, 480, 640, 0, 0)
    cv2.imwrite("Images/UnwrapedImage/CibleNet1.jpg", cibleNet)
    