
from PyQt5 import QtCore, QtGui, QtWidgets

import SaveSQL


class Ui_Dialog(object):
    def setupUi(self, Dialog,nom):
        self.nom=nom
        Dialog.resize(757, 457)
        self.label_image = QtWidgets.QLabel(Dialog)
        self.label_image.setGeometry(QtCore.QRect(30, 110, 291, 281))
        self.label_image.setText("")
        try:
            text="image/joueur/"+nom+".jpg"
            print(text)
            self.label_image.setPixmap(QtGui.QPixmap(text))
        except:
            print("ERREUR DANS LA LECTURE DE L'IMAGE")
        self.label_image.setScaledContents(True)
        self.label_image.setWordWrap(False)
        self.label_image.setObjectName("label_image")
        self.label_nom = QtWidgets.QLabel(Dialog)
        self.label_nom.setGeometry(QtCore.QRect(20, 10, 221, 71))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label_nom.setFont(font)
        self.label_nom.setObjectName("label_nom")
        self.label_nombre_partie = QtWidgets.QLabel(Dialog)
        self.label_nombre_partie.setGeometry(QtCore.QRect(370, 150, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label_nombre_partie.setFont(font)
        self.label_nombre_partie.setObjectName("label_nombre_partie")
        self.label_nombre_P_Moy = QtWidgets.QLabel(Dialog)
        self.label_nombre_P_Moy.setGeometry(QtCore.QRect(370, 300, 271, 41))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label_nombre_P_Moy.setFont(font)
        self.label_nombre_P_Moy.setObjectName("label_nombre_P_Moy")
        self.label_nombre_vict = QtWidgets.QLabel(Dialog)
        self.label_nombre_vict.setGeometry(QtCore.QRect(370, 200, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label_nombre_vict.setFont(font)
        self.label_nombre_vict.setObjectName("label_nombre_vict")
        self.label_Ratio = QtWidgets.QLabel(Dialog)
        self.label_Ratio.setGeometry(QtCore.QRect(370, 250, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label_Ratio.setFont(font)
        self.label_Ratio.setObjectName("label_Ratio")
        self.label_nombre_partie_2 = QtWidgets.QLabel(Dialog)
        self.label_nombre_partie_2.setGeometry(QtCore.QRect(670, 150, 71, 41))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label_nombre_partie_2.setFont(font)
        self.label_nombre_partie_2.setObjectName("label_nombre_partie_2")
        self.label_nombre_vict_2 = QtWidgets.QLabel(Dialog)
        self.label_nombre_vict_2.setGeometry(QtCore.QRect(670, 200, 71, 41))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label_nombre_vict_2.setFont(font)
        self.label_nombre_vict_2.setObjectName("label_nombre_vict_2")
        self.label_Ratio_2 = QtWidgets.QLabel(Dialog)
        self.label_Ratio_2.setGeometry(QtCore.QRect(670, 250, 51, 41))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label_Ratio_2.setFont(font)
        self.label_Ratio_2.setObjectName("label_Ratio_2")
        self.label_nombre_P_Moy_2 = QtWidgets.QLabel(Dialog)
        self.label_nombre_P_Moy_2.setGeometry(QtCore.QRect(670, 300, 51, 41))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label_nombre_P_Moy_2.setFont(font)
        self.label_nombre_P_Moy_2.setObjectName("label_nombre_P_Moy_2")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label_nom.setText(_translate("Dialog", self.nom))
        self.label_nombre_partie.setText(_translate("Dialog", "Nombre de partie :"))
        self.label_nombre_P_Moy.setText(_translate("Dialog", "Nombre de pers moy :"))
        self.label_nombre_vict.setText(_translate("Dialog", "Nombre de victoire :"))
        self.label_Ratio.setText(_translate("Dialog", "Ratio de victoire :"))
        nbre_Partie=SaveSQL.getPartie(self.nom)
        self.label_nombre_partie_2.setText(_translate("Dialog", str(nbre_Partie)))
        nbre_Vict=SaveSQL.getVictory(self.nom)
        self.label_nombre_vict_2.setText(_translate("Dialog", str(nbre_Vict)))
        ratio=nbre_Vict/nbre_Partie
        self.label_Ratio_2.setText(_translate("Dialog", str(ratio)))
        nbre_P_Moy=SaveSQL.get_nbre_P_Moy(self.nom)
        self.label_nombre_P_Moy_2.setText(_translate("Dialog", str(nbre_P_Moy)))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog,"Ayoub")
    Dialog.show()
    sys.exit(app.exec_())
